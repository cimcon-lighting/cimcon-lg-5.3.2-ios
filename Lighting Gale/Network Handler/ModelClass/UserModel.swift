//
//  UserModel.swift
//  SONY
//
//  Created by admin on 07/03/17.
//  Copyright © 2017 e-Procurement Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class UserModel : NSObject, NSCoding {
    
    var strClientId       : String?
    var strUserId         : String?
    var strFName          : String?
    var strUName          : String?
    var strAccessToken    : String?
    var strTokenType      : String?
    var strPhone          : String?
    var strClienttype     : String?
    var strLat            : String?
    var strLng            : String?
    var strDtFrmat        : String?
    
    override init () {
        
    }
    
    //  Encode User Data
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.strClientId,     forKey: CLIENTID)
        aCoder.encode(self.strFName,        forKey: USERFULLNAME)
        aCoder.encode(self.strUName,        forKey: UNAME)
        aCoder.encode(self.strUserId,       forKey: USERID)
        aCoder.encode(self.strAccessToken,  forKey: ACCESS_TOKEN)
        aCoder.encode(self.strTokenType,    forKey: TOKEN_TYPE)
        aCoder.encode(self.strPhone,        forKey: PHONE_NUMBER)
        aCoder.encode(self.strClienttype,   forKey: CLIENT_TYPE)
        aCoder.encode(self.strLat,          forKey: MIN_LAT)
        aCoder.encode(self.strLng,          forKey: MIN_LNG)
        aCoder.encode(self.strDtFrmat,      forKey: "clientDateFormate")
    }
    
    //    Decode Preference Data
    required init?(coder decoder: NSCoder) {
        self.strClientId            = decoder.decodeObject(forKey: CLIENTID)        as? String
        self.strFName               = decoder.decodeObject(forKey: USERFULLNAME)    as? String
        self.strUName               = decoder.decodeObject(forKey: UNAME)           as? String
        self.strUserId              = decoder.decodeObject(forKey: USERID)          as? String
        self.strAccessToken         = decoder.decodeObject(forKey: ACCESS_TOKEN)    as? String
        self.strTokenType           = decoder.decodeObject(forKey: TOKEN_TYPE)      as? String
        self.strPhone               = decoder.decodeObject(forKey: PHONE_NUMBER)    as? String
        self.strClienttype          = decoder.decodeObject(forKey: CLIENT_TYPE)     as? String
        self.strLat                 = decoder.decodeObject(forKey: MIN_LAT)         as? String
        self.strLng                 = decoder.decodeObject(forKey: MIN_LNG)         as? String
        self.strDtFrmat             = decoder.decodeObject(forKey: "clientDateFormate") as? String
//
    }
    
    // Save User Data
    func setUserData(_ saveData: [String : AnyObject]) {
        self.strClientId            = getStringFromAnyObject(saveData[CLIENTID])
        self.strFName               = getStringFromAnyObject(saveData[USERFULLNAME])
        self.strUName               = getStringFromAnyObject(saveData[UNAME])
        self.strUserId              = getStringFromAnyObject(saveData[USERID])
        self.strAccessToken         = getStringFromAnyObject(saveData[ACCESS_TOKEN])
        self.strTokenType           = getStringFromAnyObject(saveData[TOKEN_TYPE])
        self.strPhone               = getStringFromAnyObject(saveData[PHONE_NUMBER])
        self.strClienttype          = getStringFromAnyObject(saveData[CLIENT_TYPE])
        self.strLat                 = getStringFromAnyObject(saveData[MIN_LAT])
        self.strLng                 = getStringFromAnyObject(saveData[MIN_LNG])
        self.strDtFrmat             = getStringFromAnyObject(saveData["clientDateFormate"])
        UserdefaultManager().setUserModel(self)
    }    
}
