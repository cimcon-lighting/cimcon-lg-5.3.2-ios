//
//  AlertView.swift
//  SLC Admin
//
//  Created by Apple on 09/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class AlertView: NSObject {
    
    //ShowAlert
    func showAlert (strMsg : String?, btntext : String, completion: @escaping (_ btnpress : String) -> Void)  {
        let alert = UIAlertController(title: APPNAME, message: strMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: btntext, style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                completion(btntext)
            case .cancel:
                print("cancel")
                completion("CANCEL")
            case .destructive:
                print("destructive")
                completion("destructive")                
            }}))
        appDelegate.window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
}
