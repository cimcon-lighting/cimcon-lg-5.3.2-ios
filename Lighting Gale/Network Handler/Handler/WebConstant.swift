//
//  WebConstant.swift
//  SLC Admin
//
//  Created by Apple on 09/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

//WEB CONSTANT
let APPNAME                     =       "LightingGale"
let STATUS                      =       "status"
let MSG                         =       "message"
let DETAILS                     =       "details"
let OK                          =       "OK"
let DATA                        =       "data"
let NETWRKERR                   =       "No Internet Connection"
let USERMODEL                   =       "userModel"
let APPLANGUAGE                 =       "appLanguage"
let LIST                        =       "list"
let EMAIL                       =       "Email"
let SECURITY                    =       "SECUTIRY"


//LOGIN
let UNAME_SMALL                 =       "username"
let UNAME                       =       "userName"
let PWD                         =       "password"
let GRANT_TYPE                  =       "grant_type"
let DEFAULT_PWD                 =       "password"
let ACCESS_TOKEN                =       "access_token"
let TOKEN_TYPE                  =       "token_type"
let USERID                      =       "userid"
let CLIENTID                    =       "clientid"
let PHONE_NUMBER                =       "phoneNumber"
let CLIENT_TYPE                 =       "clientType"
let USERFULLNAME                =       "userfullname"


//SENT COMMAND
let CONTENT_TYPE                =       "Content-Type"
let AUTHORIZATION               =       "Authorization"
let PAGE_START_INDEX            =       "PageStartIndex"
let SENT_COMMAND_OBJ            =       "SendCommandobj"
let PAGE_SIZE                   =       "PageSize"
let PAGE_NO                     =       "pageno"
let TRACK_LIST                  =       "trackNetworkList"
let TOTAL_RECORDS               =       "totalRecords"
let SENT_BY                     =       "sentBy"
let CMD_NM                      =       "commandname"
let SEND_STATUS                 =       "sendstatus"
let TRACK_ID                    =       "trackID"
let DETAILS_CMD                 =       "detail"
let TIME                        =       "createdon"
let RESPORT_TYPE                =       "ReportType"
let DATE_TIME                   =       "datetime"
let LAST_COMMUNICATION_ON       =       "lastCommunicatedOn"


//STATUS LIST
let COMM_FAULT                  =       "8&0"
let DRIVER_FAULT                =       "9&1,8&1"
let PHOTO_CELL                  =       "2&1,8&1"
let LAMP_OFF                    =       "1&0,8&1"
let LAMP_ON_STATUS              =       "1&1,8&1"
let LAMP_ON_POWER               =       "6&=&0,|6&=&100"
let LAMP_DIM_STATUS             =       "1&1,8&1"
let LAMP_DIM_POWER              =       "6&>&0,6&<&100"
let MODE_MANUAL                 =       "9&=&0"
let PHOTOCELL                   =       "9&=&1"
let SCHEDULE                    =       "9&=&2"
let ASTRO_CLOCK                 =       "9&=&3"
let ASTRO_CLOCK_OVERRIDE        =       "9&=&4"
let MIXED_MODE                  =       "9&=&7"


let G_ID                        =       "gId"
let SLC_ID                      =       "slcId"
let SLC_GROUP                   =       "slcGroup"
let POWER_PARAM                 =       "powerPara"
let STATUS_PARAM                =       "statusPara"

//MAP
let COMMUNICATION_FAULT         =       "CommunicationFaults"
let GATEWWAY_DISC               =       "GatewayDisconnected"
let LAMPS_OFF                   =       "LampsOff"
let LAMPS_ON                    =       "LampsOn"
let LAMPS_DIME                  =       "LampsDimmed"
let GATEQWAY_CONNECTED          =       "GatewaysConnected"
let MIN_LAT                     =       "minX"
let MIN_LNG                     =       "minY"
let MAX_LAT                     =       "maxX"
let MAX_LNG                     =       "maxY"
let LATITUDE                    =       "Latitude"
let LONGITUDE                   =       "Longitude"
let DISTANCE                    =       "Distance"



//DASHBOARD
let CONENCTED                   =       "connected"
let DISCONENCTED                =       "disconnected"
let POWERLOSS                   =       "powerLoss"
let SLC_ON                      =       "slcStatusON"
let SLC_OFF                     =       "slcStatusOFF"
let SLC_DIM                     =       "slcStatusDIM"
let SLC_OK                      =       "slcok"
let SLC_REQUEST                 =       "slcStatusSerRequest"
let SLC_COMM_YES                =       "communicating"
let SLC_COMM_NO                 =       "nonCommunicating"
let SLC_COMM_NEVER              =       "neverCommunicated"
let SLC_PHOTOCELL               =       "slcModePhotocell"
let SLC_MODE_MANUAL             =       "slcModeManual"
let SLC_MIXED_MODE              =       "slcModeMixedMode"
let SLC_ASTRO_OVERRIDE          =       "slcModeAstroClockwithOverride"
let SLC_ASTRO                   =       "slcModeAstroClock"
let SLC_SCHEDULE                =       "slcModeScheduled"
let SLC_NO                      =       "SLCNo"
let CMD                         =       "Command"
let SLC_LIST_ID                 =       "SLCIdList"
let SWITCH_ON_OFF               =       "SwitchONOFF"
let RESET_SLC_TRIP              =       "ResetSLCTrip"
let READ_DATA                   =       "ReadDataList"
let DIM_VALUE                   =       "DIMValue"

/*
 
 slcStatusON": 0,
 "slcStatusOFF": 0,
 "slcStatusDIM": 0,
 "slcStatusok": 0,
 "slcStatusSerRequest": 0,
 "slcok": 0,
 "communicating": 0,
 "nonCommunicating": 16,
 "neverCommunicated": 32,
 "slcModePhotocell": 0,
 "slcModeManual": 0,
 "slcModeMixedMode": 0,
 "slcModeAstroClockwithOverride": 0,
 "slcModeScheduled": 0,
 "slcModeAstroClock": 0,
 "slcModeCivilTwlight": 0,
 "slcModeCivilTwlightwithPhotocellOverride": 0,
 "slcModeCommunicationFault": 29,
 "slcModeNodatareceived": 31
 
 */




