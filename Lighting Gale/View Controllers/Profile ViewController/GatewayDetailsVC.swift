//
//  GatewayDetailsVC.swift
//  Lighting Gale
//
//  Created by Apple on 03/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import MapKit

import FirebaseAnalytics

class CellGDetails : UITableViewCell {
    @IBOutlet var lblHeading    : UILabel!
    @IBOutlet var lblValue      : UILabel!
}

class GatewayDetailsVC: LGParent {

    var dictMainData            : [String : AnyObject] = [:]
    
    var arrSection              : [String] = []
    
    var arrSection1             : [[String : String]] = []
    var arrSection2             : [[String : String]] = []
    var arrSection3             : [[String : String]] = []
    
    var latitude                : String!
    var longitude               : String!
    
    @IBOutlet var btnOK         : UIButton!
    @IBOutlet var btnDirection  : UIButton!
    
    @IBOutlet var tblView       : UITableView!
    
    //MARK:- ViewDidload
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftBarButton(imgLeftbarButton: "back")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        btnOK.setTitle("OK".localized(), for: .normal)
        btnDirection.setTitle("Get Directions".localized(), for: .normal)
        
        arrSection =  ["Communication Parameters".localized(),
                       "RF Parameters".localized(),
                       "Properties".localized()]
        
        arrSection1 = []
        arrSection2 = []
        arrSection3 = []
        
        title = "Gateway Description".localized()
        
        var dict1 : [String : String] = [:]
        dict1["key"]      = "APN".localized()
        dict1["value"]    = (dictMainData["details"]!["apnNo"] as! String)
        
        /*var dict2 : [String : String] = [:]
        dict2["key"]      = "User ID"
        dict2["value"]    = (dictMainData["apnNo"] as! String)
        
        var dict3 : [String : String] = [:]
        dict3["key"]      = "Password"
        dict3["value"]    = (dictMainData["apnNo"] as! String)*/
        
        var dict4 : [String : String] = [:]
        dict4["key"]      = "Mobile Number".localized()
        dict4["value"]    = (dictMainData["details"]!["mobileNumber"] as! String)
        
        var dict5 : [String : String] = [:]
        dict5["key"]      = "IP".localized()
        dict5["value"]    = (dictMainData["details"]!["ipAddress"] as! String)
        
        var dict6 : [String : String] = [:]
        dict6["key"]      = "Port".localized()
        dict6["value"]    = (dictMainData["details"]!["portAddress"] as! String)
        
        var dict7 : [String : String] = [:]
        dict7["key"]      = "Extended PAN ID".localized()
        dict7["value"]    = (dictMainData["details"]!["extendedPAN"] as! String)
        
        var dict8 : [String : String] = [:]
        dict8["key"]      = "Short PAN ID".localized()
        dict8["value"]    = (dictMainData["details"]!["shortPAN"] as! String)
        
        var dict9 : [String : String] = [:]
        dict9["key"]      = "Channel Number".localized()
        dict9["value"]    = (dictMainData["details"]!["scanChannel"] as! String)
        
        var dict10 : [String : String] = [:]
        dict10["key"]      = "Latitude".localized()
        dict10["value"]    = (dictMainData["details"]!["latitude"] as! String)
        latitude = (dictMainData["details"]!["latitude"] as! String)
        
        var dict11 : [String : String] = [:]
        dict11["key"]      = "Longitude".localized()
        dict11["value"]    = (dictMainData["details"]!["longitude"] as! String)
        longitude = (dictMainData["details"]!["longitude"] as! String)
        
        arrSection1.append(dict1)
        arrSection1.append(dict4)
        arrSection1.append(dict5)
        arrSection1.append(dict6)
        arrSection2.append(dict7)
        arrSection2.append(dict8)
        arrSection2.append(dict9)
        arrSection3.append(dict10)
        arrSection3.append(dict11)
        
        tblView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Analytics.setScreenName("gatewayDetails", screenClass: "gatewayDetails")
    }
    
    //MARK:- Button click Events
    @IBAction func btnOk_Click(sender : UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDirection_Click(sender : UIButton) {
        openMapForPlace()
    }
    func openMapForPlace() {
        if latitude == "" || longitude == "" {
            AlertView().showAlert(strMsg: "Gateway location is not found".localized(), btntext: "OK".localized()) { (str) in
            }
            return
        }
        let coordinate = CLLocationCoordinate2DMake(Double(latitude)!,Double(longitude)!)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = "Target location".localized()
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
}


extension GatewayDetailsVC : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrSection1.count
        } else if section == 1 {
            return arrSection2.count
        } else {
            return arrSection3.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellGDetails = tableView.dequeueReusableCell(withIdentifier: "CellGDetails") as! CellGDetails
        var dictData : [String : String] = [:]
        if indexPath.section == 0 {
           dictData  = arrSection1[indexPath.row]
        } else if indexPath.section == 1 {
            dictData  = arrSection2[indexPath.row]
        } else if indexPath.section == 2 {
            dictData  = arrSection3[indexPath.row]
        }
        cell.lblHeading.text    = (dictData["key"]!)
        cell.lblValue.text      = (dictData["value"]!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 10, y: 10, width: tableView.frame.width-20, height: 35))
        headerView.backgroundColor = UIColor.white
        let label = UILabel.init(frame: CGRect(x: 15, y: 15, width: tableView.frame.size.width - 30, height: 20))
        label.text = arrSection[section]
        label.textColor = UIColor.darkGray//UIColor.init(red: 39.0/255.0, green: 170.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        label.textAlignment = .left
        label.font = UIFont.init(name: "Avenir-Medium", size: 17.0)
        
        let bottomlineView = UIView.init(frame: CGRect.init(x: 10, y: 38, width: tableView.frame.width-20, height: 1))
        bottomlineView.backgroundColor = UIColor.gray//UIColor.init(red: 39.0/255.0, green: 170.0/255.0, blue: 12.0/255.0, alpha: 1.0)
        
        headerView.addSubview(label)
        headerView.addSubview(bottomlineView)
        
        return headerView
    }
}
