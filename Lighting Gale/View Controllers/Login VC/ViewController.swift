//
//  ViewController.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics
import LocalAuthentication
import SwiftKeychainWrapper

import SkyFloatingLabelTextField

class ViewController: LGParent {
    
    @IBOutlet var txtUNm         : UITextField!
    @IBOutlet var txtPwd         : UITextField!
    var clientID                 : String!
    var securityCode             : String!
    
    @IBOutlet var btnRemember    : UIButton!
    @IBOutlet var btnLogin       : UIButton!
    @IBOutlet var btnTouchID     : UIButton!
    @IBOutlet var btnForgot      : UIButton!
    
    @IBOutlet var lblWC          : UILabel!
    
    @IBOutlet var consMainViewheight      : NSLayoutConstraint!
    @IBOutlet var consBtnheight           : NSLayoutConstraint!
    
    var context                 = LAContext()
    
    var isFrmFaceID              : Bool!
    var isFrmIDPWD               : Bool!
    
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        
        //        self.navigationController?.isNavigationBarHidden = true
        self.addLeftBarButton(imgLeftbarButton: "back")
        
        txtUNm.text = (UserdefaultManager().getPreferenceForkey(UNAME) as? String)
        //        txtPwd.text = (UserdefaultManager().getPreferenceForkey(PWD) as? String)
        
        if (UserdefaultManager().getPreferenceForkey(UNAME) as? String) != "" {
            btnRemember.isSelected = true
        }
    }
    
    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        setupLocalization()
    }
    
    
    
    func setupLocalization() {
        //lblWC.text              = "Welcome to".localized()
        txtUNm.placeholder      = "Email".localized()
        txtPwd.placeholder      = "Password".localized()
        var strTitel : String = ""
        if faceIDAvailable() {
            strTitel = "Login with Face ID"
        } else {
            strTitel = "Login with Touch ID"
        }
        btnRemember.setTitle("  Remember Me".localized(), for: .normal)
        btnForgot.setTitle("Forgot Password?".localized(), for: .normal)
        btnLogin.setTitle("Login".localized(), for: .normal)
        btnTouchID.setTitle(strTitel.localized(), for: .normal)
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("login", screenClass: "login")
    }
    
    
    
    
    
    func faceIDAvailable() -> Bool {
        if #available(iOS 11.0, *) {
            let context = LAContext()
            return (context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthentication, error: nil) && context.biometryType == .faceID)
        }
        return false
    }
    
    //MARK:- forgotPWD
    func forgotPWD(strEmail : String) {
        
        var dictData : [String : AnyObject] = [:]
        dictData[EMAIL] = strEmail as AnyObject
        
        ProfileServices().forgotPwd(parameters: dictData, headerParams: [:], showLoader: true) { (responseData, isSuccess) in
            if isSuccess{
                AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: OK.localized(), completion: { (str) in
                })
            }
        }
    }
    
    /*func getSAMLToken(strEmail : String) {
        var dictHeader : [String : String] = [:]
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        
        dictHeader[UNAME_SMALL]     = strEmail
        dictHeader[GRANT_TYPE]      = DEFAULT_PWD
        dictHeader["Origins"]       = "Mobile"
        dictHeader["Version"]       = version
        dictHeader["Source"]        = "IOS"
        
        ProfileServices().getSAMLToken(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess{
                UserDefaults().removeObject(forKey: "DYNAMIC")
                UserDefault.removeSuite(named: "LAMPNAME")
                UserDefaults().removeObject(forKey:"DEFAULTLAMP")
                UserDefaults().removeObject(forKey:"DEFAULTCLIENT")
                UserDefaults().removeObject(forKey:"DEFAULTCLIENT")
                UserDefaults().removeObject(forKey:"DEFAULTCLIENTID")
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_login"
                ])
                self.navigateToDashboard()
                
            }
        }
    }*/
    
    
    func navigateToDashboard() {
        if self.btnRemember.isSelected {
            UserdefaultManager().setPreference(self.clientID as AnyObject, strKey: "ClientID")
            UserdefaultManager().setPreference(self.securityCode as AnyObject, strKey: SECURITY)
            UserdefaultManager().setPreference(self.txtUNm!.text as AnyObject, strKey: UNAME)
            //                        UserdefaultManager().setPreference(self.txtPwd!.text as AnyObject, strKey: PWD)
        } else {
            UserdefaultManager().setPreference("" as AnyObject, strKey: "ClientID")
            UserdefaultManager().setPreference("" as AnyObject, strKey: SECURITY)
            UserdefaultManager().setPreference("" as AnyObject, strKey: UNAME)
            //                        UserdefaultManager().setPreference("" as AnyObject, strKey: PWD)
        }
        
        self.txtUNm.text = ""
        self.txtPwd.text = ""
        
        self.navigationController?.isNavigationBarHidden = true
        self.performSegue(withIdentifier: "loginIdentifier", sender: self)
    }
    
    //MARK:- BUTTON CLICKS
    @IBAction func  btnEye_Click(sender : UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtPwd.isSecureTextEntry = false
        } else {
            txtPwd.isSecureTextEntry = true
        }
    }
    @IBAction func remeberME_Click(sender : UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnTouchID(sender : UIButton) {
        txtUNm.resignFirstResponder()
        txtPwd.resignFirstResponder()
        if txtPwd.text == "" {
            if let retrievedPassword: String = KeychainWrapper.standard.string(forKey: txtUNm.text!) {
                if retrievedPassword != "" {
                    self.txtPwd.text = retrievedPassword
                }
            }
        }
        if isvalid() {
            self.context = LAContext()
            self.context.localizedCancelTitle = "Enter Username/Password".localized()
            var error: NSError?
            if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                let reason = "Log in to your account".localized()
                self.context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                    if success {
                        //DispatchQueue.main.async { [unowned self] in
                        //AlertView().showAlert(strMsg: "AUTHENICATION", btntext: "OK", completion: { (str) in
                        DispatchQueue.main.async {
                            if let password = self.txtPwd.text {
                                KeychainWrapper.standard.set(password, forKey: self.txtUNm.text!)
                                self.bthLoginClick(sender: self.btnRemember)
                            }
                        }
                        //})
                        //}
                    } else {
                        DispatchQueue.main.async {
                            self.txtPwd.text = ""
                            AlertView().showAlert(strMsg: "Please enter your password to continue".localized(), btntext: OK.localized(), completion: { (str) in
                                self.txtPwd.becomeFirstResponder()
                            })
                        }
                    }
                }
            } else {
                //DispatchQueue.main.async {
                self.txtPwd.text = ""
                //}
                AlertView().showAlert(strMsg: "Passcode or FaceID is not set on the device".localized(), btntext: OK.localized(), completion: { (str) in
                })
            }
        }
    }
    
        
    @IBAction func btnAzure_Click(sender : UIButton) {
        /*logout()
        self.loadCurrentAccount { (account) in
            
            guard let currentAccount = account else {
                self.acquireTokenInteractively()
                return
            }
            self.acquireTokenSilently(currentAccount)
        }*/
    }
    
    @IBAction func bthLoginClick(sender : UIButton) {
        
        txtUNm.resignFirstResponder()
        txtPwd.resignFirstResponder()
        
        if isvalid() {
            var dictHeader : [String : String] = [:]
            
            let dictionary = Bundle.main.infoDictionary!
            let version = dictionary["CFBundleShortVersionString"] as! String
            
            dictHeader[UNAME_SMALL]     = txtUNm.text
            dictHeader[PWD]             = txtPwd.text
            dictHeader[GRANT_TYPE]      = DEFAULT_PWD
            dictHeader["ClientID"]      = clientID
            dictHeader["Origins"]       = "Mobile"
            dictHeader["Version"]       = version
            dictHeader["Source"]        = "IOS"
            
            let dictParams : [String : AnyObject] = [:]
            
            ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    UserDefaults().removeObject(forKey: "DYNAMIC")
                    UserDefault.removeSuite(named: "LAMPNAME")
                    UserDefaults().removeObject(forKey:"DEFAULTLAMP")
                    UserDefaults().removeObject(forKey:"DEFAULTCLIENT")
                    UserDefaults().removeObject(forKey:"DEFAULTCLIENT")
                    UserDefaults().removeObject(forKey:"DEFAULTCLIENTID")
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_login"
                    ])
                    self.navigateToDashboard()
                } else {
                    //let saveSuccessful: Bool = KeychainWrapper.standard.set("", forKey: self.txtUNm.text!)
                }
            }
        }
    }
    
    /* @IBAction func bthLoginClick(sender : UIButton) {
     txtUNm.resignFirstResponder()
     txtPwd.resignFirstResponder()
     
     if isvalid() {
     var dictHeader : [String : String] = [:]
     if let retrievedPassword: String = KeychainWrapper.standard.string(forKey: txtUNm.text!) {
     if retrievedPassword != "" && !self.isFrmIDPWD {
     txtPwd.text = retrievedPassword
     }
     }
     
     dictHeader[UNAME_SMALL]     = txtUNm.text
     dictHeader[PWD]             = txtPwd.text
     dictHeader[GRANT_TYPE]      = DEFAULT_PWD
     dictHeader["ClientID"]      = clientID
     let dictParams : [String : AnyObject] = [:]
     
     ProfileServices().login(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
     if isSuccess {
     if self.isFrmFaceID {
     self.navigateToDashboard()
     } else {
     self.context = LAContext()
     self.context.localizedCancelTitle = "Enter Username/Password"
     var error: NSError?
     if self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
     let reason = "Log in to your account"
     self.context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
     if success {
     DispatchQueue.main.async { [unowned self] in
     AlertView().showAlert(strMsg: "AUTHENICATION", btntext: "OK", completion: { (str) in
     if let password = self.txtPwd.text {
     let saveSuccessful: Bool = KeychainWrapper.standard.set(password, forKey: self.txtUNm.text!)
     print("Save was successful: \(saveSuccessful)")
     self.navigateToDashboard()
     }
     })
     }
     self.isFrmIDPWD = false
     } else {
     self.isFrmIDPWD = true
     self.isFrmFaceID = true
     self.bthLoginClick(sender: self.btnRemember)
     }
     }
     } else {
     self.navigateToDashboard()
     }
     }
     } else {
     //let saveSuccessful: Bool = KeychainWrapper.standard.set("", forKey: self.txtUNm.text!)
     }
     }
     }
     }*/
    
    @IBAction func btnForgotPwd_Click(sender : UIButton) {
        let alert = UIAlertController(title: "Forgot Password?".localized(), message:"" , preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Email".localized()
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .default, handler: { [weak alert] (_) in
            
        }))
        alert.addAction(UIAlertAction(title: OK.localized(), style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if textField?.text == "" {
                AlertView().showAlert(strMsg: "Please enter email".localized(), btntext: OK.localized(), completion: { (str) in
                    self.btnForgotPwd_Click(sender: UIButton.init())
                })
            }
            if textField!.text != "" {
                self.forgotPWD(strEmail: (textField?.text!)!)
            }
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func  isvalid() -> Bool {
        (txtUNm as! SkyFloatingLabelTextField).errorMessage     = ""
        (txtPwd as! SkyFloatingLabelTextField).errorMessage         = ""
        if (txtUNm.text?.isEmpty)! {
            (txtUNm as! SkyFloatingLabelTextField).errorMessage = "Please enter email".localized()
            txtUNm.becomeFirstResponder()
            return false
        } else if (txtPwd.text?.isEmpty)! {//&& (KeychainWrapper.standard.string(forKey: "userPassword") == "") {
            (txtPwd as! SkyFloatingLabelTextField).errorMessage = "Please enter password".localized()
            txtPwd.becomeFirstResponder()
            return false
        }/*else if !(txtUNm.text?.isEmail)! {
         (txtUNm as! SkyFloatingLabelTextField).errorMessage = "Please enter valid email".localized()
         txtUNm.becomeFirstResponder()
         return false
         }*/ else {
            return true
         }
    }
}

extension ViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if txtUNm == textField {
            txtPwd.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

