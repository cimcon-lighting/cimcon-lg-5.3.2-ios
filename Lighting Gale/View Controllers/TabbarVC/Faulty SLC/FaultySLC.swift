//
//  FaultySLC.swift
//  Lighting Gale
//
//  Created by Apple on 19/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import MBProgressHUD

class FaultySLC: LGParent {
    
    @IBOutlet var vwFilter      : UIView!
    
    @IBOutlet var vwDetails     : UIView!
    
    @IBOutlet var tblView       : UITableView!
    @IBOutlet var tblView1      : UITableView!
    
    @IBOutlet var btnFirst      : UIButton!
    @IBOutlet var btnSearch     : UIButton!
    @IBOutlet var btnClear      : UIButton!
    
    @IBOutlet var vwTop         : UIView!
    @IBOutlet var vwSelectedTab : UIView!
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    
    var isFilterOpen            : Bool! = false
    
    var arrTblData              : [AnyObject]!
    var arrSelectdTag           : [Int]!
    var arrSearchData           : [String]!
    
    var totalCount              : Int! = 1
    var pageCount               : Int! = 1
    var selectedTAG             : Int! = 1
    
    var strMode                 : String!
    
    var dictDashboardData       : [String : AnyObject]!
    
    @IBOutlet var lblMsg        : UILabel!
    
    @IBOutlet var txtSLC        : UITextField!
    
    var refreshControl          : UIRefreshControl!
    
    var isFrmSelectedAll        : Bool! = false
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        lblMsg.isHidden = true
        
        arrSelectdTag = []
        
        constVWHeight.constant = 0
        
        arrTblData = []
        
        tblView.tableFooterView = UIView()
        
        addLeftBarButton(imgLeftbarButton: "back")
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "Search"), style: UIBarButtonItem.Style.done, target:self, action:#selector(searchClick))
        let rightButton1 = UIBarButtonItem(image: UIImage.init(named: "RoutForNAv"), style: UIBarButtonItem.Style.plain, target:self, action:#selector(mapIconClick))
        self.navigationItem.rightBarButtonItems = [rightButton, rightButton1]
        
        let lftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "back"), style: UIBarButtonItem.Style.done, target:self, action:#selector(leftButtonClick))
        let lftButton1: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "SLC_header"), style: UIBarButtonItem.Style.done, target:self, action:#selector(lftLampType))
        self.navigationItem.leftBarButtonItems = [lftButton, lftButton1]
        
        //        getDataServer(isLoadMore: true)
    }
    
    
    @objc func lftLampType() {
        view.endEditing(true)
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_lampType"
        ])
        
        let actionSheet = UIAlertController(title: "Select Lamp Type".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        let arrLmpList : [AnyObject] = UserDefault.value(forKey: "LAMPLIST") as! [AnyObject]
        
        for dictLampData in arrLmpList {
            let strTitle : String = dictLampData["lampType"] as! String
            if dictLampData["isDefault"] as! String == "1" {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                    self.dismiss(animated: true) {
                    }
                }))
            } else {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                    var arrNew : [AnyObject] = []
                    for i in 0..<arrLmpList.count {
                        var di = arrLmpList[i] as! [String : String]
                        if di["lampType"]! == strTitle {
                            di["isDefault"] = "1"
                        } else {
                            di["isDefault"] = "0"
                        }
                        arrNew.append(di as AnyObject)
                    }
                    UserDefault.set(arrNew, forKey: "LAMPLIST")
                    UserDefault.set(dictLampData["lampTypeID"]!, forKey: "DEFAULTLAMP")
                    UserDefault.set(dictLampData["lampType"]!, forKey: "LAMPNAME")
                    //self.dismiss(animated: true) {
                    self.getParamsFrmServer()
                    //}
                }))
            }
        }
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        
        present(actionSheet, animated: true)
        
    }
    
    
    override func leftButtonClick() {
        self.navigationController?.isNavigationBarHidden = true
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 105
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func mapIconClick() {
        routePlanner()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        btnSearch.setTitle("SEARCH".localized(), for: .normal)
        btnClear.setTitle("CLEAR".localized(), for: .normal)
        
        let selectLocalize = "Select".localized()
        
        if txtSLC.text?.localized() == selectLocalize {
            txtSLC.text = selectLocalize
        }
        
        refresh(sender: self)
        
        self.navigationController?.isNavigationBarHidden = false
        
        selectMode()
    }
    
    @objc func refresh(sender:AnyObject) {
        isFrmSelectedAll = false
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        arrSelectdTag.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
        
    }
    
    func selectMode() {
        switch strMode {
        case "2":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_communicationFault"
            ])
            self.title = "Communication Fault".localized()
        case "0":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_lampFault"
            ])
            self.title = "Lamp Fault".localized()
        case "1":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_driverFault"
            ])
            self.title = "Driver Fault".localized()
        case "3":
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_voltageFault"
            ])
            self.title = "Voltage Under Over".localized()
            
        default:
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchIdenifier" {
            let searchVC : SearchViewController = segue.destination as! SearchViewController
            searchVC.searchDelegate = self
            searchVC.strPlasHolder  = (sender as! String)
            searchVC.arrTblData     = arrSearchData
            searchVC.currentTag     = selectedTAG
        } else  if segue.identifier == "detailScreen" {
            let detailVC : DetailViewController = (segue.destination as? DetailViewController)!
            let dict : [String : AnyObject] = sender as! [String : AnyObject]
            detailVC.isFrmComm  = (dict["frmComm"] as! Bool)
            detailVC.arrTblData = (dict["array"] as! [AnyObject])
            detailVC.latitude   = (dict["lat"] as! String)
            detailVC.longitude  = (dict["lng"] as! String)
            detailVC.strSLCno   = (dict["slc"] as! String)
            detailVC.strAddress = ""
            if let strAdd = dict["address"] {
                detailVC.strAddress = (strAdd as! String)
            }
        }
    }
    
    //MARK:- routePlanner
    func routePlanner () {
        if arrSelectdTag.count > 0 {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_route"
            ])
            
            
            if arrSelectdTag.count > 10 {
                AlertView().showAlert(strMsg: "Please select up to 10 SLCs to use the Multi Stop Route Planner".localized(), btntext: "Close".localized()) { (str) in }
            } else {
                var arr : [[String : AnyObject]] = []
                var dict : [String : AnyObject] = [:]
                for locat in getLocationFrmArray() {
                    let nearLocation = appDelegate.clLocation.distance(from: locat)
                    dict["dist"] = nearLocation as AnyObject
                    dict["loca"] = locat as AnyObject
                    arr.append(dict)
                }
                
                let sortedArray=arr.sorted { (obj1 , obj2) -> Bool in
                    return (obj1["dist"] as! Double) < (obj2["dist"] as! Double)
                }
                
                var str : String = ""
                var newFinalArr : [CLLocation] = []
                for i in 0..<sortedArray.count {
                    let toLocation : CLLocation = sortedArray[i]["loca"] as! CLLocation
                    if i != 0 {
                        str += "+to:\(String(toLocation.coordinate.latitude)),\(String(toLocation.coordinate.longitude))"
                    } else {
                        str += "\(String(toLocation.coordinate.latitude)),\(String(toLocation.coordinate.longitude))"
                    }
                    newFinalArr.append(toLocation)
                }
                
                print(newFinalArr)
                
                //performSegue(withIdentifier: "mapboxIdenitifier", sender: newFinalArr)
                
                let strFnal="http://maps.google.com/?daddr="+str
                guard let url = URL(string: strFnal) else { return }
                UIApplication.shared.open(url)
            }
        } else {
            AlertView().showAlert(strMsg: "Select the SLC(s) before performing this operation".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
    func getLocationFrmArray() -> [CLLocation] {
        var arrLocation : [CLLocation] = []
        for i in 0..<arrTblData.count {
            if arrSelectdTag.contains(i) {
                if (arrTblData![i]["longitude"] as? String) != ""  {
                    let cll : CLLocation = CLLocation.init(latitude:Double(arrTblData![i]["latitude"]! as! String)!, longitude: Double(arrTblData![i]["longitude"]! as! String)!)
                    arrLocation.append(cll)
                }
            }
        }
        return arrLocation
    }
    
    //MARK:- getDataServer
    
    func getParamsFrmServer() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getLampList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                if let arrFrmServer : [AnyObject] = (responseData[DATA] as? [AnyObject]) {
                    if arrFrmServer.count > 0 {
                        let arrDynamic      : [AnyObject] = arrFrmServer
                        
                        var dictDynamic : [String : String] = [:]
                        
                        var strKey          : String = ""
                        var strValue        : String = ""
                        
                        for dict in arrDynamic {
                            strKey      = dict["type"] as! String
                            strValue    = dict["key"] as! String
                            dictDynamic[strKey] = strValue
                        }
                        UserDefaults().set(dictDynamic, forKey: "DYNAMIC")
                    }
                    self.refresh(sender: self)
                }
            }
        }
    }
    
    func getDataServer(isLoadMore : Bool) {
        
        let strSelect = "Select".localized()
        
        if arrTblData.count != totalCount {
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
            dictHeader["pageno"]        = String(pageCount)
            dictHeader["PageSize"]      = "10"
            dictHeader["FaultyPrameter"] = strMode
            dictHeader["SlcId"]         = txtSLC.text == strSelect ? "" : txtSLC.text
            
            ProfileServices().getFaultyList(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    let arrTmp : [AnyObject] = responseData[DATA]!["slcList"] as! [AnyObject]
                    self.arrTblData.append(contentsOf: arrTmp)
                    print(self.arrTblData.count)
                    self.totalCount = Int(responseData[DATA]![TOTAL_RECORDS]   as! String)
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }/* else {
             self.lblMsg.isHidden = true
             }*/
            //self.tblView.reloadData()
        }
    }
    
    @IBAction func rightButtonPlusClick() {
        self.view.addSubview(vwFilter)
        vwFilter.frame = self.view.bounds
        vwFilter.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwFilter.alpha = 1.0
        }
        
    }
    
    @IBAction func btnBGClosed_Click (sender : UIButton) {
        vwFilter.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwFilter.alpha = 0.0
        }) { (isDone) in
            self.vwFilter.removeFromSuperview()
        }
    }
    
    
    
    //MARK:- getSLCs
    func getSLCs() {
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader[SLC_NO]          = ""
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader["NodeType"]      = ""
        
        ProfileServices().getSLCList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "1")
                print(self.arrSearchData)
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "SLC#")
            }
        }
    }
    
    /*func showModal() {
     let modalViewController = DetailViewController()
     modalViewController.modalPresentationStyle = .overCurrentContext
     present(modalViewController, animated: true, completion: nil)
     }*/
    
    //MARK:- Button clicks
    @IBAction func btnCheck_Click(sender : UIButton) {
        let cell : CellStatus = tblView.cellForRow(at: IndexPath.init(row:  sender.tag, section: 0)) as! CellStatus
        
        if arrSelectdTag.contains(sender.tag) {
            let index = arrSelectdTag.index(of : sender.tag)
            arrSelectdTag.remove(at: index!)
            cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
        } else {
            arrSelectdTag.append(sender.tag)
            cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
        }
    }
    @IBAction func btnCheckAll_Click(sender : UIButton) {
        arrSelectdTag.removeAll()
        if !sender.isSelected {
            sender.isSelected = true
            isFrmSelectedAll = true
            for i in 0..<arrTblData!.count {
                arrSelectdTag.append(i)
            }
        } else {
            isFrmSelectedAll = false
            sender.isSelected = false
        }
        tblView.reloadData()
    }
    @IBAction func clearBtn_Click(sender : UIButton) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        arrSelectdTag.removeAll()
        isFrmSelectedAll    = false
        txtSLC.text       = "Select".localized()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    @IBAction func searchBtnClick (sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_searchStatusDashboard"
        ])
        isFrmSelectedAll    = false
        pageCount           = 1
        totalCount          = 1
        arrTblData.removeAll()
        arrSelectdTag.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataServer(isLoadMore: true)
        })
    }
    @IBAction func btnStatus_Click(sender : UIButton) {
        UIView.animate(withDuration: 0.1) {
            self.vwSelectedTab.frame = CGRect.init(x: sender.frame.origin.x , y: self.vwSelectedTab.frame.origin.y, width: sender.frame.width, height: self.vwSelectedTab.frame.height)
            self.view.layoutIfNeeded()
        }
        
        arrTblData.removeAll()
        totalCount  = 1
        pageCount   = 1
        tblView.reloadData()
        getDataServer(isLoadMore: true)
    }
    
    func hasSpecialCharacters(str : String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "@", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: str, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, str.count)) {
                return true
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
        
        return false
    }
    
    func getvalueFrmDict(dict : [String : AnyObject], isFrm : String) -> [String] {
        var arrTmp : [String] = []
        if isFrm == "1" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["value"] as! String)
            }
        }
        return arrTmp
    }
    
}

extension FaultySLC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView {
            return arrTblData.count
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView {
            let cell : CellStatus = tableView.dequeueReusableCell(withIdentifier: "CellStatus") as! CellStatus
            
            let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
            
            let lightGray = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
            let darkGray  = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 111/255, green: 113/255, blue: 121/255, alpha: 1)]
            
            let strDate = (dictData[LAST_COMMUNICATION_ON] as! String) == "" ? "N/A" : (dictData[DATE_TIME] as! String)
            if strDate == "N/A" {
                cell.lblDt.text = strDate
            } else {
                let arrStrDate = strDate.components(separatedBy: " ")
                let mutableAttrString1 = NSMutableAttributedString(string: arrStrDate[0]+" ", attributes: darkGray)
                let mutableAttrString2 = NSMutableAttributedString(string: arrStrDate[1], attributes: lightGray)
                
                let finalMutable = NSMutableAttributedString()
                finalMutable.append(mutableAttrString1)
                finalMutable.append(mutableAttrString2)
                
                cell.lblDt.attributedText = finalMutable
            }
            cell.lblSLCNm.text      = dictData["name"]    as? String
            if let slcNo = dictData["slcNo"] {
                cell.lblSLCNo.text = String(describing : slcNo)
            }
            
            cell.imgArrow?.isHidden = false
            /*if self.title !=  "Communication Faults" {
             }*/
            
            if indexPath.row % 2 != 0 {
                cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
            } else {
                cell.backgroundColor = UIColor.white
            }
            
            cell.btnCheck.addTarget(self, action: #selector(btnCheck_Click), for: .touchUpInside)
            cell.btnCheck.tag = indexPath.row
            
            if arrSelectdTag.contains(indexPath.row) {
                cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
            } else {
                cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
            }
            
            if indexPath.row == arrTblData.count-1 {
                getDataServer(isLoadMore: true)
            }
            
            return cell
            
        } else {
            let cell : CellFilter = tableView.dequeueReusableCell(withIdentifier: "CellFilter") as! CellFilter
            //cell.btnFilter.setTitle(arrFilter[indexPath.row], for: .normal)
            /*if indexPath.row % 2 != 0 {
             cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
             } else {
             cell.backgroundColor = UIColor.white
             }*/
            
            /*cell.btnCheck.addTarget(self, action: #selector(btnCheck_Click), for: .touchUpInside)
             cell.btnCheck.tag = indexPath.row
             
             if arrSelectdTag.contains(indexPath.row) {
             cell.btnCheck.setImage(UIImage.init(named: "check"), for: .normal)
             } else {
             cell.btnCheck.setImage(UIImage.init(named: "uncheck"), for: .normal)
             }*/
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36
        //return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell : CellStatus = tableView.cellForRow(at: indexPath) as! CellStatus
        if cell.lblDt.text != "N/A" {
            let dictDynaic : [String : String] = UserDefaults().value(forKey: "DYNAMIC") as! [String : String]
            let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
            var finalDictData : [String : AnyObject] = [:]
            var arrSymbol : [AnyObject] = []
            
            var dictTmp : [String : String] = [:]
            
            var isFrmComm : Bool = false
            
            dictTmp["KEY"]      = "SLC Name".localized()
            dictTmp["VALUE"]    = (dictData["name"] as! String)
            dictTmp["FOR"]      = "D"
            arrSymbol.append(dictTmp as AnyObject)
            
            if self.title ==  "Communication Fault".localized() {
                
                dictTmp["KEY"]      = dictDynaic["s8"]//results.last
                dictTmp["VALUE"]    = "0"//String(describing: value)
                dictTmp["FOR"]      = "A"//key.hasPrefix("a") == true ? "A" : "D"
                arrSymbol.append(dictTmp as AnyObject)
                
                isFrmComm = true
                finalDictData["array"]  = arrSymbol as AnyObject
                finalDictData["lat"]    = dictData["latitude"]
                finalDictData["lng"]    = dictData["longitude"]
                finalDictData["address"] = dictData["deviceAddress"]
                finalDictData["slc"]    = "SLC# "+String(describing:(dictData["slcNo"] as AnyObject)) as AnyObject
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
                }
            } else {
                let sortedDic = dictData.sorted { (aDic, bDic) -> Bool in
                    return aDic.key < bDic.key
                }
                for (key, val) in sortedDic  {
                    if self.hasSpecialCharacters(str: key) {
                        let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
                        dictTmp["KEY"]      = dictDynaic[results.last!]
                        if let value = val as? AnyObject {
                            dictTmp["VALUE"] = String(describing: value)
                        }
                        dictTmp["FOR"]      = results.last!.hasPrefix("s") == true ? "A" : "D"
                        arrSymbol.append(dictTmp as AnyObject)
                    }
                }
            }
            finalDictData["frmComm"] = isFrmComm as AnyObject
            finalDictData["array"]  = arrSymbol as AnyObject
            finalDictData["lat"]    = dictData["latitude"]
            finalDictData["lng"]    = dictData["longitude"]
            finalDictData["address"] = dictData["deviceAddress"]
            finalDictData["slc"]    = "SLC# "+String(describing:(dictData["slcNo"] as AnyObject)) as AnyObject
            
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tblView {
            let cell : CellStatusHeader = tableView.dequeueReusableCell(withIdentifier: "CellStatusHeader") as! CellStatusHeader
            cell.lblSLCNm?.text     = "SLC NAME".localized()
            cell.lblLastDt?.text    = "LAST UPDATED".localized()
            cell.btnCheckAll.addTarget(self, action: #selector(btnCheckAll_Click), for: .touchUpInside)
            let str : String = UserDefault.value(forKey: "LAMPNAME") as! String
            cell.lblHeader?.text = "LAMP TYPE".localized()+" : "+str
            cell.btnCheckAll.isHidden = false
            if isFrmSelectedAll {
                cell.btnCheckAll.isSelected = true
            } else {
                cell.btnCheckAll.isSelected = false
            }
            return cell.contentView
        }
        return UIView.init()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
}


extension FaultySLC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtSLC.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTAG = textField.tag
        if textField == txtSLC {
            getSLCs()
        }
        return true
    }
}


extension FaultySLC : SearchVCDelegate {
    func changeSearchParams(_ placeHolder: String, tagTextField: Int) {
        
    }
    
    func changeSearchParamsStatus(_ placeHolder: String, tagTextField: Int, strGroupID : String) {
        if tagTextField == 1 {
            txtSLC.text = placeHolder
        }
    }
}
