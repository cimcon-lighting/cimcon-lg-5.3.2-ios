//
//  DetailViewController.swift
//  Lighting Gale
//
//  Created by Apple on 08/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import MapKit
import FirebaseAnalytics


class CellDetails : UITableViewCell {
    @IBOutlet var lblHeading :      UILabel!
    @IBOutlet var lbvlValue  :      UILabel!
    @IBOutlet var vwColor    :      UIView!
}


class DetailViewController: LGParent {
    
    @IBOutlet var tblView       : UITableView!
    @IBOutlet var tblView1      : UITableView!
    
    @IBOutlet var vwFilter      : UIView!
    @IBOutlet var vwSlider      : UIView!
    
    @IBOutlet var slider        : UISlider!
    
    @IBOutlet var lblSLCNo      : UILabel!
    @IBOutlet var lblHeader     : UILabel!
    @IBOutlet var lblAddress    : UILabel!
    @IBOutlet var lblHeadAdd    : UILabel!
    @IBOutlet var lblHeadHisto  : UILabel!
    @IBOutlet var lblCommand    : UILabel!
    @IBOutlet var lblSlider     : UILabel!
    @IBOutlet var lblSentCommand : UILabel!
    
    @IBOutlet var btnHistorical : UIButton!
    @IBOutlet var btnCommand    : UIButton!
    @IBOutlet var btnOkSlider   : UIButton!
    
    var arrTblData          : [AnyObject]!
    var arrFilter,arrImg,arrImgCmd : [String]!
    
    var latitude            : String!
    var longitude           : String!
    var strSLCno            : String!
    var strGatewayID        : String!
    
    var isFrmComm           : Bool? = false
    var isFrmHistorical     : Bool? = false
    
    var totalCount          : Int! = 1
    var pageCount           : Int! = 1
    
    var strScreenNm         : String?
    var strAddress          : String!
    var strSLCID            : String?
    
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.init(name: "Avenir-Heavy", size: 14.0)!,
        .foregroundColor: UIColor.init(red: 24/255, green: 170/255, blue: 45/255, alpha: 1),
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    @IBOutlet var btnOK         : UIButton!
    @IBOutlet var btnDirection  : UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.title = "SLC Details".localized()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        tblView.tableFooterView = UIView()
        addLeftBarButton(imgLeftbarButton: "back")
        
        lblSLCNo.text   = strSLCno
        if strAddress == "" {
            lblAddress.text = "N/A"
        } else {
            lblAddress.text = strAddress
        }
        
        if strScreenNm != "" && strScreenNm != nil {
            lblHeader.text = strScreenNm
        } else {
            lblHeader.text = "SLC Details".localized()
        }
        
        strSLCID = strSLCno.replacingOccurrences(of: "SLC# ", with: "")
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_slcDetails"
        ])
        
        //btnHistorical.underline()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if !isFrmHistorical! {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lblSentCommand.text = "Sent Commands".localized()
        
        isFrmHistorical = false
        
        arrImgCmd       = ["common","commoff","commdim","Readdata", "Resetdata","route_plan","getMode","setMode"]
        
        arrFilter       = ["SWITCH ON".localized(),
                           "SWITCH OFF".localized(),
                           "DIM".localized(),
                           "READ DATA".localized(),
                           "RESET TRIP".localized(),
                           "ROUTE".localized(),
                           "GET MODE".localized(),
                           "SET MODE".localized()]
        
        lblHeadAdd.text     = "Address".localized()
        lblHeadHisto.text   = "Historical Data".localized()
        lblCommand.text     = "Command".localized()
        
        let attributeSendString = NSMutableAttributedString(string: "Send".localized(),
                                                            attributes: yourAttributes)
        btnCommand.setAttributedTitle(attributeSendString, for: .normal)
        
        
        let attributeViewString = NSMutableAttributedString(string: "View".localized(),
                                                            attributes: yourAttributes)
        btnHistorical.setAttributedTitle(attributeViewString, for: .normal)
        
        btnOK.setTitle("OK".localized(), for: .normal)
        btnDirection.setTitle("Get Directions".localized(), for: .normal)        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("slcDetails", screenClass: "slcDetails")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //            historicalData
        if segue.identifier == "sentCommand" {
        } else {
            let historyVC : HistoricalDataViewController = segue.destination as! HistoricalDataViewController
            if let newID = strGatewayID {
                historyVC.strGateway = newID
            }
            historyVC.isFrmCommFault = true
            historyVC.strSLCID   = strSLCID
        }
    }
    
    
    @objc func lftSentCommand() {
        if appDelegate.isFrmPaid == "paid" {
            isFrmHistorical = true
            self.performSegue(withIdentifier: "sentCommand", sender: self)
        } else {
            showUpgardeAlert()
        }
    }
    
    func ShowSentAlertWithYs(strMsg : String) {
        let alertController: UIAlertController = UIAlertController(title: APPNAME, message: strMsg, preferredStyle: .alert)
        let okButton: UIAlertAction = UIAlertAction(title: "YES".localized(), style: .default) { action -> Void in
            self.btnBGSliderClosed_Click(sender: UIButton())
            self.setDim()
        }
        let goButton: UIAlertAction = UIAlertAction(title: "NO".localized(), style: .default) { action -> Void in}
        alertController.addAction(goButton)
        alertController.addAction(okButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func ShowSentAlert(strMsg : String) {
        let alertController: UIAlertController = UIAlertController(title: APPNAME, message: strMsg, preferredStyle: .alert)
        let okButton: UIAlertAction = UIAlertAction(title: "OK".localized(), style: .default) { action -> Void in}
        let goButton: UIAlertAction = UIAlertAction(title: "GO".localized(), style: .default) { action -> Void in
            self.lftSentCommand()
        }
        alertController.addAction(okButton)
        alertController.addAction(goButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    /*func getStringFrmArray() -> String {
     var arrIds : [String] = []
     for i in 0..<arrTblData.count {
     if arrSelectdTag.contains(i) {
     arrIds.append(String(describing: arrTblData![i]["slcNo"] as AnyObject))
     }
     }
     let stringRepresentation = arrIds.joined(separator: ",")
     return stringRepresentation
     }*/
    
    @IBAction func sliderChanged(_ sender : Any){
        print(slider.value)
        print(String(describing: Int(slider.value))+" %")
        lblSlider.text = String(describing: Int(slider.value))+" %"
    }
    
    //MARK:- button clicks
    @IBAction func leftButtonClick(sender : UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSlider_OK_Click(sender : UIButton) {
        if slider.value > 71 {
            ShowSentAlertWithYs(strMsg: "You have selected more than 70% dimming value. The visibility might decrease significantly. Are you sure you want to continue ?".localized())
        } else {
            btnBGSliderClosed_Click(sender: sender)
            setDim()
        }
    }
    
    @IBAction func btnBGSliderClosed_Click (sender : UIButton) {
        vwSlider.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwSlider.alpha = 0.0
        }) { (isDone) in
            self.slider.value = 0
            self.lblSlider.text = String(describing: Int(self.slider.value))+" %"
            self.vwSlider.removeFromSuperview()
        }
    }
    
    @IBAction func btnBGClosed_Click (sender : UIButton) {
        vwFilter.alpha = 1.0
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIView.animate(withDuration: 0.15, animations: {
            self.vwFilter.alpha = 0.0
        }) { (isDone) in
            self.vwFilter.removeFromSuperview()
        }
    }
    
    @IBAction func btnCommand_Click(sender : UIButton) {
        self.view.addSubview(vwFilter)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        vwFilter.frame = self.view.bounds
        vwFilter.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwFilter.alpha = 1.0
        }
    }
    
    @IBAction func btnHistorical_Click(sender : UIButton) {
        isFrmHistorical = true
        performSegue(withIdentifier: "historicalData", sender: self)
    }
    
    //MARK:- Button click Events
    @IBAction func btnOk_Click(sender : UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDirection_Click(sender : UIButton) {
        openMapForPlace()
    }
    func openMapForPlace() {
        if latitude == "" || longitude == "" {
            AlertView().showAlert(strMsg: "SLC Location is not found".localized(), btntext: "OK".localized()) { (str) in
                
            }
            return
        }
        let coordinate = CLLocationCoordinate2DMake(Double(latitude)!,Double(longitude)!)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = "Target location".localized()
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
    }
    
    func getModeFrmString (str : String) -> String {
        switch str {
        case "0":
            return "Manual".localized()
        case "1":
            return "Photocell".localized()
        case "2":
            return "Schedule".localized()
        case "3":
            return "Astro Clock".localized()
        case "4":
            return "Astro Clock with Photocell Override".localized()
        case "5":
            return "Civil Twilight".localized()
        case "6":
            return "Civil Twilight with Photocell Override".localized()
        case "7":
            return "Mixed Mode".localized()
        default:
            return ""
        }
    }
    
    func sliderVwOpen() {
        
        self.view.addSubview(vwSlider)
        vwSlider.frame = self.view.bounds
        vwSlider.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwSlider.alpha = 1.0
        }
        
    }
    
    func checkSelectedSLCs(strType : String, isFrmDim : Bool) {
        if isFrmDim {
            sliderVwOpen()
        } else {
            self.setOnOff(str_CMD: strType)
        }
    }
    
    //MARK:- DIM
    func setDim() {
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_dim"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        dictTmp[DIM_VALUE]      = (lblSlider.text)?.replacingOccurrences(of: " %", with: "")
        dictTmp[SLC_LIST_ID]    = strSLCID//getStringFrmArray()
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams["DIMSLC"] = arrTmp as AnyObject
        
        ProfileServices().switchDim(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            //self.refresh(sender: self)
                        })
                    }
                }
            }
        }
    }
    
    //MARK:- On Off
    func setOnOff(str_CMD : String) {
        var strEvents : String = ""
        if str_CMD == "ON" {
            strEvents = "switchOn"
        } else {
            strEvents = "switchOff"
        }
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_\(strEvents)"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        dictTmp[CMD]            = str_CMD
        dictTmp[SLC_LIST_ID]    = strSLCID//getStringFrmArray()
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams[SWITCH_ON_OFF] = arrTmp as AnyObject
        
        ProfileServices().switchOnOff(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            //self.refresh(sender: self)
                        })
                    }
                }
            }
        }
    }
    
    //MARK:- Read Data
    func setReadData() {
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_readData"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        //dictTmp[CMD]            = "ON"
        dictTmp[SLC_LIST_ID]    = strSLCID
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams[READ_DATA]   = arrTmp as AnyObject
        
        ProfileServices().getReadData(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            //self.refresh(sender: self)
                        })
                    }
                }
            }
        }
    }
    
    //MARK: - Reset Data
    func setResetData() {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_resetTrip"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        dictTmp[CMD]            = "Reset SLC Trip"
        dictTmp[SLC_LIST_ID]    = strSLCID//getStringFrmArray()
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams[RESET_SLC_TRIP] = arrTmp as AnyObject
        
        ProfileServices().setResetData(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            //self.refresh(sender: self)
                        })
                    }
                }
            }
        }
    }
    
    //MARK:- GETMODE
    func getMode() {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_getMode"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        //        dictTmp[CMD]            = "Get Mode"
        dictTmp[SLC_LIST_ID]    = strSLCID //getStringFrmArray()
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams["GetMode"] = arrTmp as AnyObject
        
        ProfileServices().getMode(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            //self.refresh(sender: self)
                        })
                    }
                }
            }
        }
    }
    
    
    //MARK:- setMode
    func setMode(strModeID : String) {
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_setMode"
        ])
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        var dictTmp : [String : String] = [:]
        dictTmp["ModeTypeValue"]  = strModeID
        dictTmp[SLC_LIST_ID]    = strSLCID//getStringFrmArray()
        let arrTmp : [AnyObject] = [dictTmp as AnyObject]
        
        var dictParams : [String : AnyObject] = [:]
        dictParams["SetModeSLC"] = arrTmp as AnyObject
        
        ProfileServices().setMode(parameters: dictParams, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                let str1 = "The command has been queued.".localized()
                let str2 = "Tap on Go below to track this command from Sent Commands screen.".localized()
                let str3 = "Track ID".localized()+": "
                
                let arrTmp : [AnyObject] = responseData["trackingIDs"] as! [AnyObject]
                if arrTmp.count > 1 {
                    self.ShowSentAlert(strMsg: "\(str1) \n\n \(str2)")
                } else {
                    if arrTmp.count != 0 {
                        self.ShowSentAlert(strMsg: "\(str1) \n\n \(str3) \(arrTmp[0]) \n\n \(str2)")
                    } else {
                        AlertView().showAlert(strMsg: responseData[MSG] as? String, btntext: "OK".localized(), completion: { (str) in
                            //self.refresh(sender: self)
                        })
                    }
                }
            }
        }
    }
    
    //MARK:- getDataforGetMode
    func getDataforGetMode() {
        
        let dictDta : [String : AnyObject] = [:]
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getSetMode(parameters: dictDta, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let dictData : [AnyObject] = responseData[DATA] as! [AnyObject]
                self.actionMapSheet(arrData: dictData)
            }
        }
    }
    
    //MARK:- ActionMapSheet
    func actionMapSheet(arrData : [AnyObject]) {
        view.endEditing(true)
        
        let actionSheet = UIAlertController(title: "Select Mode".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        for dict in arrData {
            actionSheet.addAction(UIAlertAction(title: (dict["modeName"] as! String), style: .default, handler: { action in
                self.dismiss(animated: true) {
                }
                for di in arrData {
                    
                    if action.title == (di["modeName"] as! String) {
                        if action.title == "Manual" {
                            let msgForAlert = "Setting the mode to Manual will cause the light to use the ON/OFF state it was last in when set in Manual mode. Please verify the state of the light with a Read Data command and refresh the Status page or Map to ensure light is in the desired ON/OFF state.".localized()
                            let msgForAlert1 = "Select \"OK\" to continue or \"Cancel\" to stop the command".localized()
                            self.showAlert(strTitle: "\(msgForAlert) \n\n \(msgForAlert1)", modeID: (di["modeID"] as! String))
                        } else {
                            self.setMode(strModeID: (di["modeID"] as! String))
                        }
                    }
                }
            }))
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        present(actionSheet, animated: true)
    }
    
    func showAlert(strTitle : String, modeID : String) {
        let alertController = UIAlertController(title: APPNAME, message: strTitle, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.setMode(strModeID: modeID)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}


extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView1 {
            return arrFilter.count
        } else {
            return arrTblData.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView1 {
            let cell : CellFilter = tableView.dequeueReusableCell(withIdentifier: "CellFilter") as! CellFilter
            cell.btnFilter.setTitle(arrFilter[indexPath.row], for: .normal)
            cell.imgView.image = UIImage.init(named: arrImgCmd[indexPath.row])
            if indexPath.row == 0 {
                //                cell.btnFilter.backgroundColor = UIColor.init(red: 39.0/255.0, green: 170.0/255.0, blue: 12.0/255.0, alpha: 1.0)
                cell.btnFilter.backgroundColor = UIColor.init(red: 74.0/255.0, green: 183.0/255.0, blue: 84.0/255.0, alpha: 1.0)
            } else if indexPath.row == 1 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 99.0/255.0, green: 100.0/255.0, blue: 102.0/255.0, alpha: 1.0)
            } else if indexPath.row == 2 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 252.0/255.0, green: 127.0/255.0, blue: 49.0/255.0, alpha: 1.0)
            } else if indexPath.row == 3 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 246/255.0, green: 74/255.0, blue: 46/255.0, alpha: 1.0)
            } else if indexPath.row == 4 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 174.0/255.0, blue: 283.0/255.0, alpha: 1.0)
            } else if indexPath.row == 5 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 202/255.0, blue: 200/255.0, alpha: 1.0)
            } else if indexPath.row == 6 {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 114.0/255.0, blue: 188.0/255.0, alpha: 1.0)
            } else {
                cell.btnFilter.backgroundColor = UIColor.init(red: 0/255.0, green: 65.0/255.0, blue: 111.0/255.0, alpha: 1.0)
            }
            return cell
        } else {
            let cell : CellDetails = tableView.dequeueReusableCell(withIdentifier: "CellDetails") as! CellDetails
            
            let dictDynaic : [String : String] = UserDefaults().value(forKey: "DYNAMIC") as! [String : String]
            
            let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
            cell.lblHeading.text    = dictData["KEY"] as? String
            print(dictData)
            if dictData["FOR"] as! String == "A" {
                cell.lbvlValue.text = " "
                cell.vwColor.isHidden = false
                cell.lbvlValue.isHidden = false
                if (cell.lblHeading.text == dictDynaic["s1"] || cell.lblHeading.text == dictDynaic["s8"]) {
                    if (dictData["VALUE"] as! String) == "0" ||  (dictData["VALUE"] as! String) == "" {
                        cell.vwColor.backgroundColor = UIColor.init(red: 186.0/255.0, green: 12.0/255.0, blue: 29.0/255.0, alpha: 1.0)
                    } else {
                        cell.vwColor.backgroundColor = UIColor.init(red: 76.0/255.0, green: 169.0/255.0, blue: 80.0/255.0, alpha: 1.0)
                    }
                } else {
                    if (dictData["VALUE"] as! String) == "" {
                        cell.vwColor.isHidden = true
                        cell.lbvlValue.isHidden = true
                    } else {
                        if (dictData["VALUE"] as! String) != "0" {
                            cell.vwColor.backgroundColor = UIColor.init(red: 186.0/255.0, green: 12.0/255.0, blue: 29.0/255.0, alpha: 1.0)
                        } else {
                            cell.vwColor.backgroundColor = UIColor.init(red: 76.0/255.0, green: 169.0/255.0, blue: 80.0/255.0, alpha: 1.0)
                        }
                    }
                }
            } else {
                cell.vwColor.isHidden = true
                if cell.lblHeading.text == dictDynaic["p9"] {
                    cell.lbvlValue.text = getModeFrmString(str: dictData["VALUE"] as! String).localized()
                } else {
                    cell.lbvlValue.text = (dictData["VALUE"] as! String)
                }
            }
            
            /*if indexPath.row % 2 != 0 {
             cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
             } else {
             cell.backgroundColor = UIColor.white
             }*/
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tblView1 {
            btnBGClosed_Click(sender: btnOkSlider)
            if indexPath.row == 0 {
                self.checkSelectedSLCs(strType: "ON", isFrmDim: false)
            } else if indexPath.row == 1 {
                self.checkSelectedSLCs(strType: "OFF", isFrmDim: false)
            } else if indexPath.row == 2 {
                self.checkSelectedSLCs(strType: "OFF", isFrmDim: true)
            } else if indexPath.row == 3 {
                self.setReadData()
            } else if indexPath.row == 4 {
                self.setResetData()
            } else if indexPath.row == 5 {
                self.btnDirection_Click(sender: btnDirection)
                //self.routePlanner()
            } else if indexPath.row == 6 {
                self.getMode()
            } else if indexPath.row == 7 {
                self.getDataforGetMode()
            }
        }
    }
}
