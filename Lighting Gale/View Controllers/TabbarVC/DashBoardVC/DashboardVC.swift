//
//  DashboardVC.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics
import EasyNotificationBadge

class CellDashboard: UITableViewCell {
    @IBOutlet var btnNm     : UIButton!
    @IBOutlet var btnKey    : UIButton!
    @IBOutlet var imgView   : UIImageView?
}

class CellCollectionView: UICollectionViewCell {
    @IBOutlet var lblName   : UILabel!
    @IBOutlet var imgView   : UIImageView?
}

class DashboardVC: LGParent {
    
    @IBOutlet var imgStatus         : UIImageView!
    
    @IBOutlet var lblStatusOK         : UILabel!
    @IBOutlet var lblStatusService    : UILabel!
    @IBOutlet var lblLitOn            : UILabel!
    @IBOutlet var lblLitOff           : UILabel!
    @IBOutlet var lblLitDim           : UILabel!
    @IBOutlet var lblCommYes          : UILabel!
    @IBOutlet var lblCommNo           : UILabel!
    @IBOutlet var lblCommNever        : UILabel!
    @IBOutlet var lblPhotoCell        : UILabel!
    @IBOutlet var lblManual           : UILabel!
    @IBOutlet var lblMixedMode        : UILabel!
    @IBOutlet var lblAstroClockOvr    : UILabel!
    @IBOutlet var lblScheduled        : UILabel!
    @IBOutlet var lblAstroClock       : UILabel!
    //@IBOutlet var lblTotal            : UILabel!
    @IBOutlet var lblLS               : UILabel!
    @IBOutlet var lblST               : UILabel!
    @IBOutlet var lblMODE             : UILabel!
    @IBOutlet var lblCOMM             : UILabel!
    
    @IBOutlet var lblON               : UILabel!
    @IBOutlet var lblOFF              : UILabel!
    @IBOutlet var lblDIM              : UILabel!
    //    @IBOutlet var lblTOT              : UILabel!
    @IBOutlet var lblOK               : UILabel!
    @IBOutlet var lblSERV             : UILabel!
    @IBOutlet var lblYES              : UILabel!
    @IBOutlet var lblNO               : UILabel!
    @IBOutlet var lblNEVER            : UILabel!
    
    
    @IBOutlet var vwLight             : UIView!
    @IBOutlet var vwStatus            : UIView!
    @IBOutlet var vwMode              : UIView!
    @IBOutlet var vwComm              : UIView!
    
    @IBOutlet var btnON               : UIButton!
    @IBOutlet var btnOFF              : UIButton!
    @IBOutlet var btnDIM              : UIButton!
    @IBOutlet var btnVWOK             : UIButton!
    @IBOutlet var btnTotal            : UIButton!
    
    @IBOutlet var collectionVw             : UICollectionView!
    
    @IBOutlet var vwInformation       : UIView!
    
    var arrTblData                    : [[String : String]]!
    
    var isFrmOption                   : String!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrTblData = [["Key" : "OK",
                       "Value" : "Ok-1"],
                      ["Key" : "Astro Clock with Override",
                       "Value" : "03"],
                      ["Key" : "Service",
                       "Value" : "close"],
                      ["Key" : "Astro Clock",
                       "Value" : "04"],
                      ["Key" : "Photocell",
                       "Value" : "01"],
                      ["Key" : "Mixed Mode",
                       "Value" : "05"],
                      ["Key" : "Schedule",
                       "Value" : "02"],
                      ["Key" : "Manual",
                       "Value" : "06"],
                      ["Key" : "Driver Faults",
                       "Value" : "status_2"],
                      ["Key" : "Gateway On",
                       "Value" : "gon"],
                      ["Key" : "Lamp On",
                       "Value" : "dashboard_On"],
                      ["Key" : "Gateway Off",
                       "Value" : "goff"],
                      ["Key" : "Lamp Dim",
                       "Value" : "dashboard_Dim"],
                      ["Key" : "Gateway Power Lost",
                       "Value" : "gpowerloss"],
                      ["Key" : "Lamp Off",
                       "Value" : "dashboard_Off"],
                      ["Key" : "Lamp Type",
                       "Value" : "lmpType"],
                      ["Key" : "Never Communicated",
                       "Value" : "P"],
                      ["Key" : "Communication Fault",
                       "Value" : "cfail"],
                      ["Key" : "YES",
                       "Value" : "slc_yes"]]
        
        getClientType()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        setupLocalization()
        getDataFrmServer()
        collectionVw.reloadData()
    }
    
    func setupLocalization() {
        lblLS.text      = "LAMP STATUS".localized()
        lblST.text      = "SLC STATUS".localized()
        lblMODE.text    = "SLC MODE".localized()
        lblCOMM.text    = "SLC COMM. STATUS".localized()
        lblON.text      = "OK".localized()
        lblOFF.text     = "SERVICE".localized()
        /*lblOK.text      = OK.localized()
        //        lblTOT.text     = "TOTAL".localized()
        lblSERV.text    = "SERVICE".localized()
        lblDIM.text     = "DIM".localized()
        lblYES.text     = "YES".localized()
        lblNO.text      = "NO".localized()
        lblNEVER.text   = "NEVER".localized()*/
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("dashboard", screenClass: "dashboard")
    }
    
    func getClientType() {
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getClientType(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                var arrClientType = [["clientType" : "ALL".localized(), "value" : ""], ["clientType" : "Unknown", "value" : "0"]]
                let arrReposnedata = responseData[DATA] as! [AnyObject]
                for dict in arrReposnedata {
                    arrClientType.append(dict as! [String : String])
                }
                if arrClientType.count > 0 {
                    print(arrClientType)
                    
                    UserDefault.set(arrClientType, forKey: "CLIENTTYPE")
                    
                    let clientNm = arrClientType.first?["clientType"]
                    let clientID = arrClientType.first?["value"]
                    
                    UserDefault.set(clientNm, forKey: "DEFAULTCLIENT")
                    UserDefault.set(clientID, forKey: "DEFAULTCLIENTID")
                }
            }
        }
    }
    
    func getDataFrmServer() {
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        if UserDefaults().value(forKey: "DYNAMIC") == nil {
            dictHeader["PType"]         = "1"
        } else {
            dictHeader["PType"]         = ""
        }
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getDashBoard(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let dictData : [String : AnyObject] = responseData[DATA] as! [String : AnyObject]
                self.lblStatusOK.text        = dictData[SLC_OK]             as? String
                self.lblStatusService.text   = dictData[SLC_REQUEST]        as? String
                self.lblLitOn.text           = dictData[SLC_ON]             as? String
                self.lblLitOff.text          = dictData[SLC_OFF]            as? String
                self.lblLitDim.text          = dictData[SLC_DIM]            as? String
                self.lblCommNo.text          = dictData[SLC_COMM_NO]     as? String
                self.lblCommNever.text       = dictData[SLC_COMM_NEVER]        as? String
                self.lblCommYes.text         = dictData[SLC_COMM_YES]       as? String
                self.lblPhotoCell.text       = dictData[SLC_PHOTOCELL]      as? String
                self.lblManual.text          = dictData[SLC_MODE_MANUAL]    as? String
                self.lblMixedMode.text       = dictData[SLC_MIXED_MODE]     as? String
                self.lblAstroClock.text      = dictData[SLC_ASTRO]          as? String
                self.lblAstroClockOvr.text   = dictData[SLC_ASTRO_OVERRIDE] as? String
                self.lblScheduled.text       = dictData[SLC_SCHEDULE]       as? String
                
                let totalNumber = Int(dictData["slcTotalOK"] as! String)
                let numberFormatter = NumberFormatter()
                
                numberFormatter.locale = Locale.init(identifier: UserdefaultManager().getPreferenceForkey(APPLANGUAGE) as! String)
                numberFormatter.usesGroupingSeparator = true
                numberFormatter.numberStyle = .decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value:totalNumber!))
                
                self.btnTotal.setTitle("\("Total SLC".localized()) - "+(formattedNumber!), for: .normal)
                
                if UserDefault.value(forKey: "DEFAULTLAMP") == nil {
                    if let arrLamplist : [AnyObject] = dictData["lamplist"] as? [AnyObject] {
                        UserDefault.set(arrLamplist, forKey: "LAMPLIST")
                        for dictList in arrLamplist {
                            if dictList["isDefault"] as! String == "1" {
                                UserDefault.set(dictList["lampTypeID"]!, forKey: "DEFAULTLAMP")
                                UserDefault.set(dictList["lampType"]!, forKey: "LAMPNAME")
                                self.getParamsFrmServer()
                            }
                        }
                    }
                }
                
                var badgeAppearance = BadgeAppearance()
                badgeAppearance.textAlignment = .center //default is center
                badgeAppearance.textSize = 10
                
                var totalSLC : Int = 0
                totalSLC +=  Int((dictData[SLC_OK] as? String)!)!
                totalSLC +=  Int((dictData[SLC_REQUEST] as? String)!)!
            }
        }
    }
    
    
    func getParamsFrmServer() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getLampList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                if let arrFrmServer : [AnyObject] = responseData[DATA] as? [AnyObject] {
                    if arrFrmServer.count > 0 {
                        let arrDynamic      : [AnyObject] = arrFrmServer
                        
                        var dictDynamic : [String : String] = [:]
                        
                        var strKey          : String = ""
                        var strValue        : String = ""
                        
                        for dict in arrDynamic {
                            strKey      = dict["type"] as! String
                            strValue    = dict["key"] as! String
                            dictDynamic[strKey] = strValue
                        }
                        UserDefaults().set(dictDynamic, forKey: "DYNAMIC")
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chartIdentifier" {
            let chartVC : ChartViewController = segue.destination as! ChartViewController
            chartVC.isFrmOption = isFrmOption
            chartVC.arrValue = sender as! [Double]
        } else  if segue.identifier == "statusDashboard" {
            let statusDashVC : StatusDashBoardVC = segue.destination as! StatusDashBoardVC
            statusDashVC.strMode = ((sender as! [String : AnyObject])["MODE"] as! String)
            statusDashVC.dictDashboardData = (sender as! [String : AnyObject])
        }
    }
    
    
    //MARK:- Button clicks events
    @IBAction func btnInfo_Click(sender : UIButton) {
        collectionVw.reloadData()
        self.view.addSubview(vwInformation)
        vwInformation.frame = self.view.bounds
        vwInformation.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwInformation.alpha = 1.0
        }
    }
    
   /* override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.vwInformation.removeFromSuperview()
    }*/
    
    @IBAction func btnBGClosed_Click (sender : UIButton) {
        vwInformation.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwInformation.alpha = 0.0
        }) { (isDone) in
            self.vwInformation.removeFromSuperview()
        }
    }
    
    @IBAction func btnLightAll_Click(sender : UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblLitOn.text != "0" || lblLitOff.text != "0" || lblLitDim.text != "0" {
                var arrTmp : [Double] = []
                
                arrTmp.append((Double(lblLitOn.text!))!)
                arrTmp.append((Double(lblLitOff.text!))!)
                arrTmp.append((Double(lblLitDim.text!))!)
                
                isFrmOption = "SLCLIGHT"
                
                performSegue(withIdentifier: "chartIdentifier", sender: arrTmp)
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnSLC_Mode(sender : UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblAstroClock.text != "0" || lblScheduled.text != "0" || lblMixedMode.text != "0" || lblManual.text != "0" || lblAstroClockOvr.text != "0" || lblPhotoCell.text != "0" {
                var arrTmp : [Double] = []
                arrTmp.append((Double(lblPhotoCell.text!))!)
                arrTmp.append((Double(lblScheduled.text!))!)
                arrTmp.append((Double(lblAstroClockOvr.text!))!)
                arrTmp.append((Double(lblAstroClock.text!))!)
                arrTmp.append((Double(lblMixedMode.text!))!)
                arrTmp.append((Double(lblManual.text!))!)
                
                isFrmOption = "SLCMODE"
                
                performSegue(withIdentifier: "chartIdentifier", sender: arrTmp)
            }
        } else {
            showUpgardeAlert()
        }
        /*self.lblPhotoCell.text       = dictData[SLC_PHOTOCELL]      as? String
         self.lblManual.text          = dictData[SLC_MODE_MANUAL]    as? String
         self.lblMixedMode.text       = dictData[SLC_MIXED_MODE]     as? String
         self.lblAstroClock.text      = dictData[SLC_ASTRO]          as? String
         self.lblAstroClockOvr.text   = dictData[SLC_ASTRO_OVERRIDE] as? String
         self.lblScheduled.text       = dictData[SLC_SCHEDULE]       as? String*/
    }
    
    @IBAction func btnTotal_Click(sender : UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_total"
            ])
            
            var dictData : [String : AnyObject] = [:]
            dictData["MODE"] = "9" as AnyObject
            performSegue(withIdentifier: "statusDashboard", sender: dictData)
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnOK_Click(_ sender: UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblStatusOK.text != "0" {
                
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_statusOk"
                ])
                
                var dictData : [String : AnyObject] = [:]
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["MODE"]  = "10"                 as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                performSegue(withIdentifier: "statusDashboard", sender: dictData)
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnServices(_ sender: UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblStatusService.text != "0" {
                var dictHeader : [String : String] = [:]
                dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
                dictHeader[CONTENT_TYPE]    = "application/json"
                
                ProfileServices().getFaultyCount(parameters: [:], headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
                    if isSuccess{
                        let dictresponseData : [String : AnyObject] = responseData[DATA] as! [String : AnyObject]
                        
                        self.isFrmOption = "SLCFAULT"
                        
                        var arrTmp : [Double] = []
                        //arrTmp.append((Double(lblPhotoCell.text!))!)
                        //arrTmp.append((Double(dictresponseData["communicationFault"] as! String))!)
                        arrTmp.append((Double(dictresponseData["lampFault"] as! String))!)
                        arrTmp.append((Double(dictresponseData["driverFault"] as! String))!)
                        arrTmp.append((Double(dictresponseData["lowVoltage"] as! String))!)
                        arrTmp.append((Double(dictresponseData["dayBurner"] as! String))!)
                        arrTmp.append((Double(dictresponseData["outages"] as! String))!)
                        
                        self.performSegue(withIdentifier: "chartIdentifier", sender: arrTmp)
                    }
                }
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnCommunication(sender : UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblCommYes.text != "0" || lblCommNo.text != "0" || lblCommNever.text != "0" {
                var arrTmp : [Double] = []
                
                arrTmp.append((Double(lblCommYes.text!))!)
                arrTmp.append((Double(lblCommNo.text!))!)
                arrTmp.append((Double(lblCommNever.text!))!)
                
                isFrmOption = "SLCOMMUNICATION"
                
                performSegue(withIdentifier: "chartIdentifier", sender: arrTmp)
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnYes(_ sender: UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblCommYes.text != "0" {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_statusYes"
                ])
                
                var dictData : [String : AnyObject] = [:]
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&1"              as AnyObject
                dictData["MODE"]  = "12"                as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                performSegue(withIdentifier: "statusDashboard", sender: dictData)
            }
        } else {
            showUpgardeAlert()
        }
    }
    @IBAction func btnNo(_ sender: UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblCommNo.text != "0" {
                
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_statusNo"
                ])
                
                var dictData : [String : AnyObject] = [:]
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 0                   as AnyObject
                dictData["STATUS"] = "8&0"              as AnyObject
                dictData["MODE"]  = "13"                as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                performSegue(withIdentifier: "statusDashboard", sender: dictData)
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    @IBAction func btnNever(_ sender: UIButton) {
        if appDelegate.isFrmPaid == "paid" {
            if lblCommNever.text != "0" {
                
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_statusNever"
                ])
                
                var dictData : [String : AnyObject] = [:]
                dictData["POWER"] = ""                  as AnyObject
                dictData["NEVER"] = 1                   as AnyObject
                dictData["STATUS"] = ""                 as AnyObject
                dictData["MODE"]  = "14"                as AnyObject
                dictData["ServiceRequest"] = ""         as AnyObject
                performSegue(withIdentifier: "statusDashboard", sender: dictData)
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    /*@IBAction func btnON_Click(sender : UIButton) {
     if lblLitOn.text != "0"  {
     Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
     AnalyticsParameterItemName: "statusOn"
     ])
     
     var dictData : [String : AnyObject] = [:]
     dictData["POWER"] = LAMP_ON_POWER       as AnyObject
     dictData["NEVER"] = 0                   as AnyObject
     dictData["STATUS"] = LAMP_ON_STATUS     as AnyObject
     dictData["MODE"]  = "6"                 as AnyObject
     dictData["ServiceRequest"] = ""         as AnyObject
     performSegue(withIdentifier: "statusDashboard", sender: dictData)
     }
     }
     
     @IBAction func btnOFF_Click(sender : UIButton) {
     if lblLitOff.text != "0" {
     
     Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
     AnalyticsParameterItemName: "statusOff"
     ])
     
     var dictData : [String : AnyObject] = [:]
     dictData["POWER"] = ""                  as AnyObject
     dictData["NEVER"] = 0                   as AnyObject
     dictData["STATUS"] = LAMP_OFF           as AnyObject
     dictData["MODE"]  = "7"                 as AnyObject
     dictData["ServiceRequest"] = ""         as AnyObject
     performSegue(withIdentifier: "statusDashboard", sender: dictData)
     }
     }
     
     @IBAction func btnDIM_Click(sender : UIButton) {
     if lblLitDim.text != "0" {
     Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
     AnalyticsParameterItemName: "statusDim"
     ])
     
     var dictData : [String : AnyObject] = [:]
     dictData["POWER"] = LAMP_DIM_POWER      as AnyObject
     dictData["NEVER"] = 0                   as AnyObject
     dictData["STATUS"] = LAMP_DIM_STATUS    as AnyObject
     dictData["MODE"]  = "8"                 as AnyObject
     dictData["ServiceRequest"] = ""         as AnyObject
     performSegue(withIdentifier: "statusDashboard", sender: dictData)
     }
     }*/
}


extension DashboardVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellDashboard = tableView.dequeueReusableCell(withIdentifier: "CellDashboard") as! CellDashboard
        cell.btnNm.setTitle(arrTblData![indexPath.row]["Value"], for: .normal)
        cell.btnKey.setTitle(arrTblData![indexPath.row]["Key"], for: .normal)
        return cell
    }
}

extension DashboardVC : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTblData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CellCollectionView  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollectionView", for: indexPath as IndexPath) as! CellCollectionView
        
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as [String : AnyObject]
        
        cell.imgView?.image = UIImage.init(named: dictData["Value"]! as! String)
        cell.lblName.text = (dictData["Key"] as? String)?.localized()
        return cell
    }
}

