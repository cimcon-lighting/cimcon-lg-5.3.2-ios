//
//  MapSLCViewController.swift
//  SLC Admin
//
//  Created by Apple on 22/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import MapKit
import GoogleMaps
import FBAnnotationClustering
import FirebaseAnalytics

class MapSLCViewController: LGParent, GMUClusterManagerDelegate {
    
    var clusterManager      : GMUClusterManager?
    
    var tileRender          : MKTileOverlayRenderer?
    
    var overLay             : MKTileOverlay?
    
    var boundingMapRect     : MKMapRect?
    
    var googlMapView        : GMSMapView?
    
    var mapRegion           : MKCoordinateRegion?
    
    var distanceInMeters    : CLLocationDistance = 0
    var currentMapTypeIndex : Int = 0
    
    var clusteringManager   : FBClusteringManager?
    
    @IBOutlet var mapView   : MKMapView!
    
    @IBOutlet var btnFault  : UIButton!
    @IBOutlet var btnOn     : UIButton!
    @IBOutlet var btnOff    : UIButton!
    @IBOutlet var btnDIm    : UIButton!
    @IBOutlet var btnP      : UIButton!
    @IBOutlet var btnGOn    : UIButton!
    @IBOutlet var btnGOff   : UIButton!
    @IBOutlet var btnPowerloss : UIButton!
    
    var finalAnn : MyAnnotation!
    
    @IBOutlet var vwOption  : UIView!
    
    @IBOutlet var vwInformation  : UIView!
    
    var arrTblData          : [String]!
    
    
    @IBOutlet var collectionVw  : UICollectionView!
    
    var arrInfo                    : [[String : String]]!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrInfo = [["Key" : "OK",
                    "Value" : "Ok-1"],
                   ["Key" : "Astro Clock with Override",
                    "Value" : "03"],
                   ["Key" : "Service",
                    "Value" : "close"],
                   ["Key" : "Astro Clock",
                    "Value" : "04"],
                   ["Key" : "Photocell",
                    "Value" : "01"],
                   ["Key" : "Mixed Mode",
                    "Value" : "05"],
                   ["Key" : "Schedule",
                    "Value" : "02"],
                   ["Key" : "Manual",
                    "Value" : "06"],
                   ["Key" : "Driver Faults",
                    "Value" : "status_2"],
                   ["Key" : "Gateway On",
                    "Value" : "gon"],
                   ["Key" : "Lamp On",
                    "Value" : "dashboard_On"],
                   ["Key" : "Gateway Off",
                    "Value" : "goff"],
                   ["Key" : "Lamp Dim",
                    "Value" : "dashboard_Dim"],
                   ["Key" : "Gateway Power Lost",
                    "Value" : "gpowerloss"],
                   ["Key" : "Lamp Off",
                    "Value" : "dashboard_Off"],
                   ["Key" : "Lamp Type",
                    "Value" : "lmpType"],
                   ["Key" : "Never Communicated",
                    "Value" : "P"],
                   ["Key" : "Communication Fault",
                    "Value" : "cfail"],
                   ["Key" : "YES",
                    "Value" : "slc_yes"]
                   ]
        
        //if self.serviceHandler.objUserModel.strClienttype == "1" {
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            let arrClientType = arr as! [[String : AnyObject]]
            let filterdArray = arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                btnGOn.isHidden = false
                btnGOff.isHidden = false
                btnPowerloss.isHidden = false
            }
        }
        
        distanceInMeters = 15.0
        
        self.mapView.isHidden = true
        
        googlMapView        = GMSMapView.init(frame:CGRect.init(x: self.view.frame.origin.x,
                                                                y: ((self.navigationController?.navigationBar.frame.size.height)!+(appDelegate.window?.safeAreaInsets.top)!),
                                                                width: view.frame.size.width,
                                                                height: view.frame.size.height-((self.navigationController?.navigationBar.frame.size.height)!+(appDelegate.window?.safeAreaInsets.top)!)-((self.tabBarController?.tabBar.frame.size.height)!)), camera: googleMapCamera())
        
        //googlMapView        = GMSMapView.init(frame: CGRect(x: , y: 0, width: view.frame.size.width, height: view.frame.size.height - 64-44), camera: camera)
        self.view.addSubview(googlMapView!)
        googlMapView?.delegate = self
        googlMapView?.animate(to: googleMapCamera())
        
        let algorithm       = GMUNonHierarchicalDistanceBasedAlgorithm.init()
        let iconGenerator   = GMUDefaultClusterIconGenerator.init()
        
        let renderer        = GMUDefaultClusterRenderer.init(mapView: googlMapView!, clusterIconGenerator: iconGenerator)
        clusterManager      = GMUClusterManager.init(map: googlMapView!, algorithm: algorithm, renderer: renderer)
        renderer.delegate   = self
        
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "SLC_header"), style: UIBarButtonItem.Style.done, target:self, action:#selector(leftButtonClick))
        self.navigationItem.leftBarButtonItems = [leftButton]
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "map-pin-marked"), style: UIBarButtonItem.Style.done, target:self, action:#selector(rghtButtonClick))
        let rightButton1: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "i"), style: UIBarButtonItem.Style.done, target:self, action:#selector(rghtInfoButtonClick))
        self.navigationItem.rightBarButtonItems = [rightButton1, rightButton]
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_map"
        ])
        
        //        addLeftBarButton(imgLeftbarButton: "map-pin-marked")
        
        /*btnFault.backgroundColor    = UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
         btnDIm.backgroundColor      = UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
         btnGOff.backgroundColor     = UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
         btnGOn.backgroundColor      = UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
         btnOn.backgroundColor       = UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
         btnOff.backgroundColor      = UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)*/
    }
    
    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        localize()
        mapSetup()
        collectionVw.reloadData()
    }
    
    func localize() {
        self.navigationItem.title  = "Map".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("map", screenClass: "map")
    }
    
    //MARK:- SETUP MAP
    func mapSetup() {
        
        googlMapView!.clear()
        
        clusterManager!.clearItems()
        
        mapView.removeAnnotations(mapView.annotations)
        
        setupAppleMap()
        
        getDataFrmServer()
        
        currentMapTypeIndex = appDelegate.strSelectedMap
        
        if (currentMapTypeIndex != 6) && (currentMapTypeIndex > 2) {
            mapView.isHidden = true
            googlMapView!.isHidden = false
            googlMapView?.animate(to: googleMapCamera())
            self.view.bringSubviewToFront(vwOption)
        } else {
            mapView.isHidden = false
            googlMapView!.isHidden = true
        }
        
        googlMapView?.isMyLocationEnabled = true
        mapView.showsUserLocation = true
        
        if currentMapTypeIndex != 6 {
            selectMap()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self.setupOSMMap()
            })
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self.selectMap()
                self.setupOSMMap()
            })
        }
        
        arrTblData = ["Apple Satellite".localized(),
                      "Apple Standard".localized(),
                      "Apple Hybrid".localized(),
                      "Google Map Satellite".localized(),
                      "Google Map Standard".localized(),
                      "Google Map Hybrid".localized(),
                      "Open Street Standard".localized()]
    }
    
    @objc func rghtInfoButtonClick() {
        collectionVw.reloadData()
        self.view.addSubview(vwInformation)
        vwInformation.frame = self.view.bounds
        vwInformation.alpha = 0.0
        UIView.animate(withDuration: 0.15) {
            self.vwInformation.alpha = 1.0
        }
    }
    
    
    @IBAction func btnBGClosed_Click (sender : UIButton) {
        vwInformation.alpha = 1.0
        UIView.animate(withDuration: 0.15, animations: {
            self.vwInformation.alpha = 0.0
        }) { (isDone) in
            self.vwInformation.removeFromSuperview()
        }
    }
    
    @objc func rghtButtonClick() {
        if appDelegate.isFrmPaid == "paid" {
            view.endEditing(true)
            
            let actionSheet = UIAlertController(title: "Select Map Type".localized(), message: "", preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
                self.dismiss(animated: true) {
                }
            }))
            
            for strTitle in arrTblData {
                if appDelegate.strSelectedMap == arrTblData.firstIndex(of: strTitle) {
                    actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                        self.dismiss(animated: true) {
                        }
                    }))
                } else {
                    actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                        if (strTitle == "Apple Satellite".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_appleSatellite"
                            ])
                        } else if (strTitle == "Apple Standard".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_appleStandard"
                            ])
                        } else if (strTitle == "Apple Hybrid".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_appleHybrid"
                            ])
                        } else if (strTitle == "Google Map Satellite".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_googleSatellite"
                            ])
                        } else if (strTitle == "Google Map Standard".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_googleStandard"
                            ])
                        } else if (strTitle == "Google Map Hybrid".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_googleHybrid"
                            ])
                        }
                        else if (strTitle == "Open Street Standard".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_openStandard"
                            ])
                        }
                        /*Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                         AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_\(strTitle)"
                         ])*/
                        appDelegate.strSelectedMap = self.arrTblData.firstIndex(of: strTitle)!
                        self.currentMapTypeIndex = appDelegate.strSelectedMap
                        self.mapSetup()
                        //self.txtMaptype.text = strTitle
                        self.dismiss(animated: true) {
                        }
                    }))
                }
            }
            if UI_USER_INTERFACE_IDIOM() == .pad {
                actionSheet.modalPresentationStyle = .popover
                let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
                popPresenter?.sourceView =  self.view;
                popPresenter?.sourceRect =  self.view.bounds;
            }
            
            present(actionSheet, animated: true)
        } else {
            showUpgardeAlert()
        }
    }
    
    
    @objc override func leftButtonClick() {
        if appDelegate.isFrmPaid == "paid" {
            view.endEditing(true)
            
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_lampType"
            ])
            
            let actionSheet = UIAlertController(title: "Select Lamp Type".localized(), message: "", preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
                self.dismiss(animated: true) {
                }
            }))
            
            let arrLmpList : [AnyObject] = UserDefault.value(forKey: "LAMPLIST") as! [AnyObject]
            
            for dictLampData in arrLmpList {
                let strTitle : String = dictLampData["lampType"] as! String
                if dictLampData["isDefault"] as! String == "1" {
                    actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                        self.dismiss(animated: true) {
                        }
                    }))
                } else {
                    actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                        var arrNew : [AnyObject] = []
                        for i in 0..<arrLmpList.count {
                            var di = arrLmpList[i] as! [String : String]
                            if di["lampType"]! == strTitle {
                                di["isDefault"] = "1"
                            } else {
                                di["isDefault"] = "0"
                            }
                            arrNew.append(di as AnyObject)
                        }
                        UserDefault.set(arrNew, forKey: "LAMPLIST")
                        UserDefault.set(dictLampData["lampTypeID"]!, forKey: "DEFAULTLAMP")
                        UserDefault.set(dictLampData["lampType"]!, forKey: "LAMPNAME")
                        //self.dismiss(animated: true) {
                        self.getParamsFrmServer()
                        //}
                    }))
                }
            }
            if UI_USER_INTERFACE_IDIOM() == .pad {
                actionSheet.modalPresentationStyle = .popover
                let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
                popPresenter?.sourceView =  self.view;
                popPresenter?.sourceRect =  self.view.bounds;
            }
            
            present(actionSheet, animated: true)
        } else {
            showUpgardeAlert()
        }
    }
    
    
    func googleMapCamera() -> GMSCameraPosition{
        var camera = GMSCameraPosition.init()
        if self.serviceHandler.objUserModel.strLat == "" {
            if appDelegate.clLocation.coordinate.latitude == 0.0 {
                showAlert()
            } else {
                camera          = GMSCameraPosition.camera(withLatitude: appDelegate.clLocation.coordinate.latitude, longitude: appDelegate.clLocation.coordinate.longitude, zoom: 16.0)
            }
        } else  {
            camera          = GMSCameraPosition.camera(withLatitude: Double(self.serviceHandler.objUserModel.strLat ?? "0.0")!, longitude: Double(self.serviceHandler.objUserModel.strLng ?? "0.0")!, zoom: 16.0)
        }
        return camera
    }
    
    func selectMap () {
        switch currentMapTypeIndex {
        case 0:
            mapView?.mapType = .satellite
        case 1:
            mapView?.mapType = .standard
        case 2:
            mapView?.mapType = .hybrid
        case 3:
            googlMapView!.mapType = .satellite
        case 4:
            googlMapView!.mapType = .terrain
        case 5:
            googlMapView!.mapType = .hybrid
        case 6:
            mapView?.reloadInputViews()
        default:
            break
        }
    }
    
    /*@objc func rghtButtonClick() {
     self.performSegue(withIdentifier: "profileIdentifier", sender: nil)
     }*/
    
    //MARK:- Get Details From SLC
    func getDetails(strPowerParam : String, strStatusParam : String, strSLC:String, nvrComm : String) {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader[PAGE_NO]         = String(1)
        dictHeader[PAGE_SIZE]       = "10"
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        
        var dictData : [String : AnyObject] = [:]
        dictData[G_ID]          = ""             as AnyObject
        dictData[SLC_ID]        = strSLC         as AnyObject
        dictData[SLC_GROUP]     = ""             as AnyObject
        dictData[POWER_PARAM]   = strPowerParam  as AnyObject
        dictData[STATUS_PARAM]  = strStatusParam as AnyObject
        dictData["NeverCommunication"]  = nvrComm == "DNR" ? 1 as AnyObject : 0 as AnyObject
        dictData["LastReceivedMode"] = "" as AnyObject
        
        ProfileServices().getStatusList(parameters: dictData, headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responseData[LIST]       as! [AnyObject]
                
                if arrTmp.count > 0 {
                    let dictData : [String : AnyObject] = arrTmp[0] as! [String : AnyObject]
                    
                    let dictDynaic : [String : String] = UserDefaults().value(forKey: "DYNAMIC") as! [String : String]
                    
                    var finalDictData : [String : AnyObject] = [:]
                    var arrSymbol : [AnyObject] = []
                    var dictTmp   : [String : String] = [:]
                    
                    dictTmp["KEY"]      = "SLC Name".localized()
                    dictTmp["VALUE"]    = (dictData["name"] as! String)
                    dictTmp["FOR"]      = "D"
                    arrSymbol.append(dictTmp as AnyObject)
                    
                    let sortedDic = dictData.sorted { (aDic, bDic) -> Bool in
                        return aDic.key < bDic.key
                    }
                    var isFrmComm : Bool = false
                    if let communication = dictData["@s8"] {
                        if communication as! String == "0" {
                            dictTmp["KEY"]      = dictDynaic["s8"]//results.last
                            dictTmp["VALUE"]    = "0"//String(describing: value)
                            dictTmp["FOR"]      = "A"//key.hasPrefix("a") == true ? "A" : "D"
                            isFrmComm = true
                            arrSymbol.append(dictTmp as AnyObject)
                        } else {
                            for (key, val) in sortedDic  {
                                if self.hasSpecialCharacters(str: key) {
                                    let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
                                    dictTmp["KEY"]      = dictDynaic[results.last!]
                                    if let value = val as? AnyObject {
                                        dictTmp["VALUE"] = String(describing: value)
                                    }
                                    dictTmp["FOR"]      = results.last!.hasPrefix("s") == true ? "A" : "D"
                                    arrSymbol.append(dictTmp as AnyObject)
                                }
                            }
                        }
                    } else {
                        for (key, val) in sortedDic  {
                            if self.hasSpecialCharacters(str: key) {
                                let results = key.components(separatedBy: NSCharacterSet(charactersIn: "@") as CharacterSet)
                                dictTmp["KEY"]      = dictDynaic[results.last!]
                                if let value = val as? AnyObject {
                                    dictTmp["VALUE"] = String(describing: value)
                                }
                                dictTmp["FOR"]      = results.last!.hasPrefix("s") == true ? "A" : "D"
                                arrSymbol.append(dictTmp as AnyObject)
                            }
                        }
                    }
                    finalDictData["frmComm"] = isFrmComm as AnyObject
                    finalDictData["array"]  = arrSymbol as AnyObject
                    finalDictData["lat"]    = dictData["latitude"]
                    finalDictData["lng"]    = dictData["longitude"]
                    finalDictData["address"] = dictData["deviceAddress"]
                    finalDictData["slc"]    = "SLC# "+String(describing:(dictData["slcNo"] as AnyObject)) as AnyObject
                    if let gID = dictData["gatewayId"] {
                        finalDictData["gateway"] = String(gID as! Int) as AnyObject
                    }
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "detailScreen", sender: finalDictData)
                    }
                } else {
                    AlertView().showAlert(strMsg: "No data Found".localized(), btntext: "OK".localized()) { (str) in}
                }
            }
        }
    }
    
    
    //MARK:- Get Gateway Details From Server
    func getGatewayDetails(strFrom : String, strGatewayID : String) {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader["pageNo"]        = String(1)
        dictHeader[PAGE_SIZE]       = "10"
        dictHeader["GatewayType"]   = strFrom
        dictHeader["GatewayName"]   = ""
        dictHeader["Gatewayid"]     = strGatewayID
        
        ProfileServices().getGatewayList(parameters: [:], headerParams: dictHeader, showLoader: true) { (responsedata, isSuccess) in
            if isSuccess {
                let arrTmp : [AnyObject] = responsedata[DATA]![LIST]       as! [AnyObject]
                if arrTmp.count > 0 {
                    let dictData : [String : AnyObject] = arrTmp[0] as! [String : AnyObject]
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "gatwayDetailsIdentifier", sender: dictData)
                    }
                } else {
                    AlertView().showAlert(strMsg: "No data Found".localized(), btntext: "OK".localized()) { (str) in}
                }
            }
        }
    }
    
    //MARK:- Get ALL DATA FROM SERVER
    func getParamsFrmServer() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getLampList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                if let arrFrmServer : [AnyObject] = (responseData[DATA] as? [AnyObject]) {
                    if arrFrmServer.count > 0 {
                        let arrDynamic      : [AnyObject] = arrFrmServer
                        
                        var dictDynamic : [String : String] = [:]
                        
                        var strKey          : String = ""
                        var strValue        : String = ""
                        
                        for dict in arrDynamic {
                            strKey      = dict["type"] as! String
                            strValue    = dict["key"] as! String
                            dictDynamic[strKey] = strValue
                        }
                        UserDefaults().set(dictDynamic, forKey: "DYNAMIC")
                    }
                    self.getDataFrmServer()
                }
            }
        }
    }
    
    func getDataFrmServer() {
        
        googlMapView!.clear()
        clusterManager!.clearItems()
        mapView.removeAnnotations(mapView.annotations)
        
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader["lamptypeid"]    = (UserDefault.value(forKey: "DEFAULTLAMP") as! String)
        
        var dictData : [String : AnyObject] = [:]
        
        if (currentMapTypeIndex != 6) && (currentMapTypeIndex > 2) {
            var bounds =  GMSCoordinateBounds.init(region: googlMapView!.projection.visibleRegion())
            dictData[MIN_LAT]  = bounds.southWest.longitude      as AnyObject
            dictData[MIN_LNG]  = bounds.southWest.latitude       as AnyObject
            dictData[MAX_LAT]  = bounds.northEast.longitude      as AnyObject
            dictData[MAX_LNG]  = bounds.northEast.latitude       as AnyObject
        } else {
            let northEast = mapView.convert(CGPoint(x: mapView.bounds.width, y: 0), toCoordinateFrom: mapView)
            let southWest = mapView.convert(CGPoint(x: 0, y: mapView.bounds.height), toCoordinateFrom: mapView)
            
            dictData[MIN_LAT]  = southWest.longitude      as AnyObject
            dictData[MIN_LNG]  = southWest.latitude       as AnyObject
            dictData[MAX_LAT]  = northEast.longitude      as AnyObject
            dictData[MAX_LNG]  = northEast.latitude       as AnyObject
        }
        
        dictData[COMMUNICATION_FAULT]   = btnFault.isSelected == true  ? "cfail"   as AnyObject   : "" as AnyObject
        dictData[GATEWWAY_DISC]         = btnGOff.isSelected  == true  ? "goff"    as AnyObject   : "" as AnyObject
        dictData[GATEQWAY_CONNECTED]    = btnGOn.isSelected   == true  ? "gon"     as AnyObject   : "" as AnyObject
        dictData[LAMPS_ON]              = btnOn.isSelected    == true  ? "on"      as AnyObject   : "" as AnyObject
        dictData[LAMPS_OFF]             = btnOff.isSelected   == true  ? "off"     as AnyObject   : "" as AnyObject
        dictData[LAMPS_DIME]            = btnDIm.isSelected   == true  ? "dim"     as AnyObject   : "" as AnyObject
        dictData["NeverCommunicated"]   = btnP.isSelected     == true  ? "DNR"     as AnyObject   : "" as AnyObject
        dictData["GatewaysPowerloss"]   = btnPowerloss.isSelected     == true  ? "gpowerloss" as AnyObject   : "" as AnyObject
        
        
        ProfileServices().getMapData(parameters: dictData, headerParams: dictHeader , showLoader: true, completion: { (responseData, isSuccess) in
            if isSuccess {
                print(responseData)
                if let arrData : [AnyObject] = (responseData[DATA] as? [AnyObject]) {
                    var arrPin : [AnyObject] = []
                    for index in 0..<arrData.count {
                        let subDic = arrData[index]
                        if self.currentMapTypeIndex != 6 && self.currentMapTypeIndex > 2 {
                            
                        } else {
                            let strLat = subDic["c"] as? Double
                            let strLng = subDic["d"] as? Double
                            
                            let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(strLat ?? 0.0, strLng ?? 0.0)
                            var title : String! = ""
                            if let object : String = subDic["g"] as? String {
                                if object == "goff"{
                                    title = "Gateway Disconnected".localized()
                                } else if object == "cfail"{
                                    title = "Communication Fail".localized()
                                } else if object == "gon"{
                                    title = "Gateway Connected".localized()
                                } else if object == "on"{
                                    title = "Lamp On".localized()
                                } else if object == "off"{
                                    title = "Lamp Off".localized()
                                } else if object == "dim" {
                                    title = "Lamp Dim".localized()
                                } else if object == "DNR" {
                                    title = "In Planning".localized()
                                } else if object == "gpowerloss" {
                                    title = "Power Lost".localized()
                                }
                            }
                            var subTitle: String! = ""
                            if (subDic["a"] as! Int ) == 1 {
                                if let object = subDic["f"] {
                                    subTitle = (object as! String)
                                }
                            } else {
                                if let object = subDic["b"] {
                                    subTitle = "SLC# "+String(describing: object!)
                                }
                            }
                            let strID       = subDic["a"] as! Double
                            let strStatus   = subDic["g"] as! String
                            let strSLCID    = subDic["b"] as! Int
                            let annot : MyAnnotation = MyAnnotation.init(coordinate: location, title: title, subtitle: subTitle, strID: strID, status: strStatus, slcID:String(strSLCID))
                            arrPin.append(annot)
                            self.mapView.addAnnotation(annot)
                            //}
                        }
                    }
                    if (self.currentMapTypeIndex != 6 && self.currentMapTypeIndex > 2) {
                        self.clusterManager!.clearItems()
                        self.generateClusterItems(arrData)
                        self.clusterManager?.cluster()
                        self.clusterManager?.setDelegate(self, mapDelegate: self)
                    } else {
                        self.clusteringManager = FBClusteringManager(annotations: arrPin)
                        self.clusteringManager!.delegate = self
                        DispatchQueue.main.async(execute: {
                            let scale = Double(self.mapView?.bounds.size.width ?? 0.0) / (self.mapView?.visibleMapRect.size.width ?? 0.0)
                            let annotations = self.clusteringManager!.clusteredAnnotations(within: (self.mapView?.visibleMapRect)!, withZoomScale: scale)
                            self.clusteringManager!.displayAnnotations(annotations, on: self.mapView)
                        })
                    }
                }
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailScreen" {
            let detailVC : DetailViewController = (segue.destination as? DetailViewController)!
            let dict : [String : AnyObject] = sender as! [String : AnyObject]
            
            detailVC.isFrmComm  = (dict["frmComm"] as! Bool)
            detailVC.arrTblData = (dict["array"] as! [AnyObject])
            detailVC.latitude   = (dict["lat"] as! String)
            detailVC.longitude  = (dict["lng"] as! String)
            detailVC.strAddress = ""
            if let strAdd = dict["address"] {
                detailVC.strAddress = (strAdd as! String)
            }
            detailVC.strSLCno   = (dict["slc"] as! String)
        } else if segue.identifier == "gatwayDetailsIdentifier"{
            let searchVC : GatewayDetailsVC = segue.destination as! GatewayDetailsVC
            searchVC.dictMainData =  sender as! [String : AnyObject]
        }
    }
    
    
    //MARK:- Button Click Events
    @IBAction func btnCFail_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapCommunicationFault"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnLampOff_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapLampOff"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.blue//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnLampOn_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapLampOn"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnLampDim_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapDim"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnPtag_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapInPlanning"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnGatewayOff_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapGatewayOff"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnGatewayOn_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapGatewayOn"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    @IBAction func btnPowerloss_Click(sender : UIButton) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_mapPowerloss"
        ])
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(red: 0/255, green: 160/255, blue: 241/255, alpha: 1)//UIColor.init(red: 186.0/255.0, green: 186.0/255.0, blue: 186.0/255.0, alpha: 1.0)
        } else {
            sender.backgroundColor = .white
        }
        getDataFrmServer()
    }
    func showAlert() {
        let alertController = UIAlertController(title: APPNAME, message: "Please enable location from Settings".localized(), preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Go To Settings".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func hasSpecialCharacters(str : String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "@", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: str, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, str.count)) {
                return true
            }
        } catch {
            debugPrint(error.localizedDescription)
            return false
        }
        
        return false
    }
    
    
    //MARK:- setupAppleMap
    func setupAppleMap() {
        var viewRegion: MKCoordinateRegion = MKCoordinateRegion.init()
        if self.serviceHandler.objUserModel.strLat == "" {
            if appDelegate.clLocation.coordinate.latitude == 0.0 {
                showAlert()
            } else {
                viewRegion = MKCoordinateRegion(center: appDelegate.clLocation.coordinate, latitudinalMeters: 3500, longitudinalMeters: 3500)
            }
        } else {
            let cllocation : CLLocation = CLLocation.init(latitude: Double(self.serviceHandler.objUserModel.strLat!) as! CLLocationDegrees, longitude: Double(self.serviceHandler.objUserModel.strLng!) as! CLLocationDegrees)
            viewRegion = MKCoordinateRegion(center: cllocation.coordinate, latitudinalMeters: 3500, longitudinalMeters: 3500)
        }
        let adjustedRegion: MKCoordinateRegion? = mapView?.regionThatFits(viewRegion)
        if let adjustedRegion = adjustedRegion {
            mapView?.setRegion(adjustedRegion, animated: true)
        }
    }
    
    func addBounceAnnimation (to : UIView) {
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [NSNumber(value: 0.05), NSNumber(value: 1.1), NSNumber(value: 0.9), NSNumber(value: 1)]
        bounceAnimation.duration = 0.6
        var timingFunctions = [AnyHashable](repeating: 0, count: bounceAnimation.values?.count ?? 0)
        
        for _ in 0..<(bounceAnimation.values?.count ?? 0) {
            timingFunctions.append(CAMediaTimingFunction(name: .easeInEaseOut))
        }
        bounceAnimation.timingFunctions = timingFunctions as? [CAMediaTimingFunction]
        bounceAnimation.isRemovedOnCompletion = false
        view.layer.add(bounceAnimation, forKey: "bounce")
    }
    
    func setupOSMMap() {
        if currentMapTypeIndex == 6 {
            overLay = MKTileOverlay.init(urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png")
            overLay!.canReplaceMapContent = true
            tileRender = MKTileOverlayRenderer.init(tileOverlay: overLay!)
            mapView.addOverlay(overLay!)
            mapView?.reloadInputViews()
            mapView.setNeedsDisplay()
        } else {
            tileRender = nil
            if overLay != nil {
                mapView?.removeOverlay(overLay!)
                overLay!.canReplaceMapContent = false
            }
            mapView.reloadInputViews()
            mapView.setNeedsDisplay()
        }
    }
}

extension MapSLCViewController : FBClusteringManagerDelegate {
    
}

extension MapSLCViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let calloutView = view.subviews.first else { return }
        if !view.isKind(of: MyCustomPinAnnotationView.self) {
            if view.annotation != nil {
                if view.annotation?.isKind(of: MyAnnotation.self) ?? false {
                    finalAnn = (view.annotation as! MyAnnotation)
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapAnnotationCalloutView(_:)))
                    calloutView.addGestureRecognizer(tapGesture)
                }
            }
        }
    }
    @objc private func userDidTapAnnotationCalloutView(_ sender: UITapGestureRecognizer) {
        if appDelegate.isFrmPaid == "paid" {
            //if control == view.rightCalloutAccessoryView {
            //let tmp = view.annotation as? MyAnnotation
            var strStatusParam  : String = ""
            var strPowerParam   : String = ""
            var neverComm       : String = ""
            if finalAnn!.status == "goff" {
                strStatusParam  = "disconnected"
            } else if  finalAnn!.status == "gon" {
                strStatusParam  = "connected"
            } else if  finalAnn!.status == "gpowerloss" {
                strStatusParam  = "powerloss"
            } else if finalAnn!.status == "DNR" {
                neverComm = "DNR"
                return
            } /*else if tmp!.status == "cfail" {
             strStatusParam  = COMM_FAULT
             strPowerParam   = ""
             } else if tmp!.status == "on" {
             strStatusParam  = LAMP_ON_STATUS
             strPowerParam   = LAMP_ON_POWER
             } else if tmp!.status == "dim" {
             strStatusParam  = LAMP_DIM_STATUS
             strPowerParam   = LAMP_DIM_POWER
             } else if tmp!.status == "off" {
             strStatusParam  = LAMP_OFF
             strPowerParam   = ""
             }*/
            if finalAnn?.sID == 1.0 {
                getGatewayDetails(strFrom: strStatusParam, strGatewayID: finalAnn!.slcID)
            } else {
                getDetails(strPowerParam: strPowerParam, strStatusParam: strStatusParam, strSLC: finalAnn!.slcID, nvrComm:neverComm )
            }
            //}
        } else {
            showUpgardeAlert()
        }
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if appDelegate.isFrmPaid == "paid" {
            //if control == view.rightCalloutAccessoryView {
            let tmp = view.annotation as? MyAnnotation
            var strStatusParam  : String = ""
            var strPowerParam   : String = ""
            var neverComm       : String = ""
            if tmp!.status == "goff" {
                strStatusParam  = "disconnected"
            }  else if  tmp!.status == "gon" {
                strStatusParam  = "connected"
            } else if  tmp!.status == "gpowerloss" {
                strStatusParam  = "powerloss"
            } else if tmp!.status == "DNR" {
                neverComm = "DNR"
                return
            } /*else if tmp!.status == "cfail" {
             strStatusParam  = COMM_FAULT
             strPowerParam   = ""
             } else if tmp!.status == "on" {
             strStatusParam  = LAMP_ON_STATUS
             strPowerParam   = LAMP_ON_POWER
             } else if tmp!.status == "dim" {
             strStatusParam  = LAMP_DIM_STATUS
             strPowerParam   = LAMP_DIM_POWER
             } else if tmp!.status == "off" {
             strStatusParam  = LAMP_OFF
             strPowerParam   = ""
             }*/
            if tmp?.sID == 1.0 {
                getGatewayDetails(strFrom: strStatusParam, strGatewayID: tmp!.slcID)
            } else {
                getDetails(strPowerParam: strPowerParam, strStatusParam: strStatusParam, strSLC: tmp!.slcID, nvrComm:neverComm )
            }
            //}
        } else {
            showUpgardeAlert()
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        for view: UIView? in views {
            //addBounceAnnimation(to: view!)
        }
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let pinIdentifier = "red_pin"
        if (annotation is MKUserLocation) {
            return nil
        } else if (annotation is FBAnnotationCluster) {
            let cluster = annotation as? FBAnnotationCluster
            print(String(format: "Annotation is cluster. Number of annotations in cluster: %lu", UInt(cluster?.annotations.count ?? 0)))
            let pin1 = MyCustomPinAnnotationView(annotation: annotation, price: Float((cluster?.annotations.count)!))
            return pin1
        } else {
            /*let pinView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdentifier)
             if pinView == nil {*/
            //let customAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdentifier)
            var annotationView: MKAnnotationView?
            //annotationView!.canShowCallout = true
            let tmp = annotation as? MyAnnotation
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdentifier) {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
                annotationView?.rightCalloutAccessoryView = UIButton(type: .infoDark)
            } else {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: pinIdentifier)
            }
            
            if let annotationView = annotationView {
                if tmp!.status != "DNR" {
                    annotationView.canShowCallout = true
                    annotationView.rightCalloutAccessoryView = UIButton(type: .infoDark)
                } else {
                    annotationView.canShowCallout = true
                    annotationView.rightCalloutAccessoryView = UIButton.init()
                }
            }
            annotationView!.image = UIImage.init(named:tmp!.status)
            
            return annotationView
            /*} else {
             //pinView?.annotation = annotation
             }*/
            //            return pinView
        }
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        mapRegion = self.mapView?.region
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return tileRender!
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        /*OperationQueue().addOperation({
         DispatchQueue.main.async(execute: {
         let mapWidth : Float        = Float(self.mapView.frame.width)
         let mapVisibleWidth : Float = Float(self.mapView.visibleMapRect.width)
         let scale  : Double         = Double((mapWidth)/(mapVisibleWidth))
         if (self.clusteringManager != nil) {
         let annotations = self.clusteringManager!.clusteredAnnotations(within: mapView.visibleMapRect, withZoomScale: Double(scale))
         self.clusteringManager!.displayAnnotations(annotations, on: mapView)
         }
         })
         })*/
        
        //DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1 * Double(NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
        self.getDataFrmServer()
        //})
        
    }
}

extension MapSLCViewController : GMSMapViewDelegate {
    
    func drawFront(_ image: UIImage?, text: String?) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(image?.size ?? CGSize.zero, _: false, _: UIScreen.main.scale)
        image?.draw(in: CGRect(x: 0, y: 0, width: image?.size.width ?? 0.0, height: image?.size.height ?? 0.0))
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        let attr = [
            NSAttributedString.Key.paragraphStyle: style,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular) /* set text font */,
            NSAttributedString.Key.foregroundColor: UIColor.white // set text color
        ]
        let rect = CGRect(x: 0, y: 19, width: image?.size.width ?? 0.0, height: image?.size.height ?? 0.0)
        text?.draw(in: rect, withAttributes: attr)
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        /*var lastZoom: Float = 0
         let currentZoom: Float = Float(googlMapView!.camera.zoom)
         if !((lastZoom - currentZoom) < FLT_EPSILON) {
         clusterManager!.clearItems()
         let bounds = GMSCoordinateBounds(region: googlMapView!.projection.visibleRegion())
         let northEast: CLLocationCoordinate2D = bounds.northEast
         let centerCoordinates: CLLocationCoordinate2D = googlMapView!.projection.coordinate(for: googlMapView!.center)
         let topCenterLocation = CLLocation(latitude: northEast.latitude, longitude: northEast.longitude)
         let centerLocation = CLLocation(latitude: centerCoordinates.latitude, longitude: centerCoordinates.longitude)
         distanceInMeters = centerLocation.distance(from: topCenterLocation) / 1000
         */
        
        //DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(1 * Double(NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
        self.getDataFrmServer()
        //})
        /*}
         lastZoom = currentZoom*/
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if appDelegate.isFrmPaid == "paid" {
            if (marker.userData is POIItem) {
                let item: POIItem? = (marker.userData as! POIItem)
                
                var strStatusParam  : String = ""
                var strPowerParam   : String = ""
                var neverComm       : String = ""
                if item!.status == "goff" {
                    strStatusParam  = "disconnected"
                } else if  item!.status == "gon" {
                    strStatusParam  = "connected"
                } else if  item!.status == "gpowerloss" {
                    strStatusParam  = "powerloss"
                } else if item!.status == "DNR" {
                    neverComm = "DNR"
                    return
                } /*else if item!.status == "cfail" {
                 strStatusParam  = COMM_FAULT
                 strPowerParam   = ""
                 } else if item!.status == "on" {
                 strStatusParam  = LAMP_ON_STATUS
                 strPowerParam   = LAMP_ON_POWER
                 } else if item!.status == "dim" {
                 strStatusParam  = LAMP_DIM_STATUS
                 strPowerParam   = LAMP_DIM_POWER
                 } else if item!.status == "off" {
                 strStatusParam  = LAMP_OFF
                 strPowerParam   = ""
                 }*/
                if item?.isFrm == 1 {
                    getGatewayDetails(strFrom: strStatusParam, strGatewayID: item!.slcID)
                } else {
                    getDetails(strPowerParam: strPowerParam, strStatusParam: strStatusParam, strSLC: item!.slcID , nvrComm:neverComm)
                }
                
                
                /*let EditView = EditDataViewController(nibName: "EditDataViewController", bundle: nil)
                 EditView.strSelecctedDataSlcID = item?.slcID
                 EditView.strEditView = strEditView
                 navigationController?.pushViewController(EditView, animated: true)*/
            }
        } else {
            showUpgardeAlert()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.title != nil {
            if let title = marker.title {
                print("Did tap marker for cluster item \(title)")
            }
        } else {
            print("Did tap a normal marker")
        }
        return false
    }
    
    func generateClusterItems(_ arrPins: [AnyObject]) {
        for index in 0..<arrPins.count {
            print(arrPins[index])
            let lat: Double = arrPins[index]["c"] as? Double ?? 0.0
            let lng: Double = arrPins[index]["d"] as? Double ?? 0.0
            var name : String = ""
            if (arrPins[index]["a"] as! Int ) == 1 {
                if let object = arrPins[index]["f"] {
                    name = (object as! String)
                }
            } else {
                if let object = arrPins[index]["b"] {
                    name = "SLC# "+String(describing: object!)
                }
            }
            
            var subTitle : String = ""
            if let object : String = arrPins[index]["g"] as? String {
                if object == "goff"{
                    subTitle = "Gateway Disconnected".localized()
                } else if object == "cfail"{
                    subTitle = "Communication Fail".localized()
                } else if object == "gon"{
                    subTitle = "Gateway Connected".localized()
                } else if object == "on"{
                    subTitle = "Lamp On".localized()
                } else if object == "off"{
                    subTitle = "Lamp Off".localized()
                } else if object == "dim" {
                    subTitle = "Lamp Dim".localized()
                } else if object == "DNR" {
                    subTitle = "In Planning".localized()
                } else if object == "gpowerloss" {
                    subTitle = "Power Lost".localized()
                }
            }
            
            let strStstus : String = arrPins[index]["g"] as! String
            let intFrom   : Int    = arrPins[index]["a"] as! Int
            
            
            let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: subTitle, snippet: name, slc: String(describing:  arrPins[index]["b"] as! Int), status:strStstus, intFrm :intFrom ) as? GMUClusterItem
            if let item = item {
                clusterManager!.add(item)
            }
        }
    }
}

extension MapSLCViewController : GMUClusterRendererDelegate {
    func clusterManager(_ clusterManager: GMUClusterManager?, didTap cluster: GMUCluster?) {
        let newCamera   = GMSCameraPosition.camera(withTarget: (cluster?.position)!, zoom: googlMapView!.camera.zoom + 1)
        let update      = GMSCameraUpdate.setCamera(newCamera)
        googlMapView!.moveCamera(update)
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if (marker.userData is POIItem) {
            let item: POIItem? = (marker.userData as! POIItem)
            if let name = item?.name, let snippet = item?.snippet {
                marker.snippet = "\(name) \n\(snippet)"
            }
            marker.icon = UIImage(named: item!.status)
        } else if (marker.userData is GMUCluster) {
            let userData: GMUCluster? = (marker.userData as! GMUCluster)
            marker.icon = drawFront(UIImage(named: "cluster_blue"), text: "\(NSNumber(value: (userData?.count)!))")
        }
    }
}

extension MapSLCViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CellCollectionView  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollectionView", for: indexPath as IndexPath) as! CellCollectionView
        
        let dictData : [String : AnyObject] = arrInfo[indexPath.row] as [String : AnyObject]
        
        cell.imgView?.image = UIImage.init(named: dictData["Value"]! as! String)
        cell.lblName.text = (dictData["Key"] as? String)?.localized()
        return cell
    }
}
