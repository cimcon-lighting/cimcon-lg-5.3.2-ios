//
//  POItem.swift
//  SLC Admin
//
//  Created by Apple on 28/02/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name    = ""
    var snippet = ""
    var slcID   = ""
    var status  = ""
    var isFrm   : Int  
    
    init(position: CLLocationCoordinate2D, name: String?, snippet sbutitle: String?, slc slcid: String?, status: String, intFrm :Int) {
        //if super.init()
        self.position   = position
        self.name       = name!
        snippet         = sbutitle ?? ""
        slcID           = slcid ?? ""
        isFrm           = intFrm
        self.status     = status
    }
}
