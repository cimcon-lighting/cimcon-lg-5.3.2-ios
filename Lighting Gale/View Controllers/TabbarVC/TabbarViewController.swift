//
//  TabbarViewController.swift
//  SLC Admin
//
//  Created by Apple on 04/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor.init(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)*/
        //func printFonts() {
        /*let fontFamilyNames = UIFont.familyNames
            for familyName in fontFamilyNames {
                print("------------------------------")
                print("Font Family Name = [\(familyName)]")
                let names = UIFont.fontNames(forFamilyName: familyName)
                print("Font Names = [\(names)]")
            }
        //}*/
        
        //Font Names = [["Avenir-Oblique", "Avenir-HeavyOblique", "Avenir-Heavy", "Avenir-BlackOblique", "Avenir-BookOblique", "Avenir-Roman", "Avenir-Medium", "Avenir-Black", "Avenir-Light", "Avenir-MediumOblique", "Avenir-Book", "Avenir-LightOblique"]]
        
        let transperentBlackColor = UIColor.clear
        
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        transperentBlackColor.setFill()
        UIRectFill(rect)
        
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            tabBar.backgroundImage = image
        }
        UITabBar.appearance().shadowImage = UIImage()
        self.tabBar.unselectedItemTintColor = UIColor.white
        
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:UIFont(name: "Avenir-Black", size: 10)]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
    }
    
    override func viewWillLayoutSubviews() {
        
        /*var tabFrame = tabBar.frame
        tabFrame.size.height = 60
        tabFrame.origin.y = self.view.frame.size.height - 60
        tabBar.frame = tabFrame*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewControllers![0].title = "DASHBOARD".localized()
        viewControllers![1].title = "STATUS".localized()
        viewControllers![2].title = "MAP".localized()
        viewControllers![3].title = "MORE".localized()
    }
}
